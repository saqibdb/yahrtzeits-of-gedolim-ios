//
//  HebrewDateConvertorViewController.m
//  Yahrtzeits of Gedolim
//
//  Created by iBuildX on 06/09/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import "HebrewDateConvertorViewController.h"

@interface HebrewDateConvertorViewController ()

@end

@implementation HebrewDateConvertorViewController

- (instancetype)initWithStyle:(RMActionControllerStyle)aStyle title:(NSString *)aTitle message:(NSString *)aMessage selectAction:(RMAction *)selectAction andCancelAction:(RMAction *)cancelAction {
    self = [super initWithStyle:aStyle title:aTitle message:aMessage selectAction:selectAction andCancelAction:cancelAction];
    if(self) {
        self.contentView = [[[NSBundle mainBundle] loadNibNamed:@"CustomDatePickerView" owner:nil options:nil] firstObject];;
        self.contentView.translatesAutoresizingMaskIntoConstraints = NO;

        
      
    
        
        NSDictionary *bindings = @{@"mapView": self.contentView};
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[mapView(>=300)]" options:0 metrics:nil views:bindings]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[mapView(300)]" options:0 metrics:nil views:bindings]];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view layoutIfNeeded];
    //self.contentView.frame = CGRectMake(0, 0, self.view.frame.size.width, 300);
    [self.view layoutIfNeeded];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)disableBlurEffectsForContentView {
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
