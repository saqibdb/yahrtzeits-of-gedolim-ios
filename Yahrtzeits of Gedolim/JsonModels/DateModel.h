//
//  DateModel.h
//  Yahrtzeits of Gedolim
//
//  Created by iBuildX on 05/06/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface DateModel : JSONModel


@property (nonatomic) NSNumber <Optional> *gy;
@property (nonatomic) NSNumber <Optional> *gm;
@property (nonatomic) NSNumber <Optional> *gd;
@property (nonatomic) NSNumber <Optional> *hy;
@property (nonatomic) NSNumber <Optional> *hd;




@property (nonatomic) NSString <Optional> *hebrew;
@property (nonatomic) NSString <Optional> *hm;


//@property (nonatomic) NSArray<NSString *, Optional>  *events;

@end

/*
 
 
 {"gy":2018,"gm":6,"gd":6,"hy":5778,"hm":"Sivan","hd":23,"hebrew":"\u05db\u05f4\u05d2 \u05d1\u05bc\u05b0\u05e1\u05b4\u05d9\u05d5\u05b8\u05df \u05ea\u05e9\u05e2\u05f4\u05d7","events":["Parashat Sh'lach"]}
 
 */
