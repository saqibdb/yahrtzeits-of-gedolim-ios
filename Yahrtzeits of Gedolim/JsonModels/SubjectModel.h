//
//  SubjectModel.h
//  Yahrtzeits of Gedolim
//
//  Created by iBuildX on 05/06/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol SubjectModel;

@interface SubjectModel : JSONModel
@property (nonatomic) NSNumber <Optional> *id;
@property (nonatomic) NSNumber <Optional> *day;
@property (nonatomic) NSNumber <Optional> *month;
@property (nonatomic) NSString <Optional> *subjectDescription;
@property (nonatomic) NSString <Optional> *subjectTitle;
@property (nonatomic) NSString <Optional> *created_date;
@property (nonatomic) NSString <Optional> *modified_date;

@end

@interface SubjectDateModel : JSONModel
@property (nonatomic) NSMutableArray<SubjectModel>  *Dates;
@property (nonatomic) NSNumber<Optional>  *Total;
@property (nonatomic) NSNumber<Optional>  *NextPage;

@end

/*
 
 "id": 16,
 "day": 10,
 "month": 5,
 "subjectTitle": "AV YUD",
 "subjectDescription": "Test Description",
 "created_date": "2018-06-07T17:12:21.996088Z",
 "modified_date": "2018-06-07T17:14:32.268580Z"
 
 */
/*
 "Total": 31,
 "NextPage": true
 
 */
