//
//  HebrewDateConvertorViewController.h
//  Yahrtzeits of Gedolim
//
//  Created by iBuildX on 06/09/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import <RMActionController/RMActionController.h>
#import "CustomDatePickerView.h"

@interface HebrewDateConvertorViewController : RMActionController

@property (weak, nonatomic) IBOutlet UIView *CustomDatePickerView;



@end
