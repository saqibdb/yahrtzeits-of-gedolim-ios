//
//  AppDelegate.m
//  Yahrtzeits of Gedolim
//
//  Created by iBuildX on 30/05/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import "AppDelegate.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "Utility.h"
#import "Yahrtzeits_of_Gedolim-Swift.h"
#import "ViewController.h"

@import UserNotifications;
@interface AppDelegate ()

@end

@implementation AppDelegate


@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionAlert)
                          completionHandler:^(BOOL granted, NSError * _Nullable error) {
                              if (!error) {
                                  if (!granted) {
                                      [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"gedolNotifications"];
                                      [[NSUserDefaults standardUserDefaults] synchronize];
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          //[SANotificationView showSAStatusBarBannerWithMessage:@"Authorization Status Denied" backgroundColor:[UIColor redColor] textColor:[UIColor whiteColor  ] showTime:3000.0];
                                          /*
                                          UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Your Notifications are Turned Off!" message:@"You did not grant the app access to receive daily notifications.  You can change this at any time within your device “Settings”." preferredStyle:UIAlertControllerStyleAlert];
                                          UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                                          [alert addAction:ok];
                                          [[self topViewController] presentViewController:alert animated:true completion:nil];
                                          
                                          */
                                      });
                                  }
                                  else{
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          [SANotificationView removeStatusView];
                                          NSLog(@"request authorization succeeded!");
                                          [Utility checkForNewDatesWithLocalHebrewDate];
                                      });
                                  }
                              }
                          }];
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"HasLaunchedOnceGedol"])
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HasLaunchedOnceGedol"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"gedolNotifications"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else{
        NSDate *lastRefreshedDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastRefreshedDate"];
        if (lastRefreshedDate) {
            NSInteger daysTillRefreshed = [Utility daysBetweenDate:lastRefreshedDate andDate:[NSDate date]];
            if (daysTillRefreshed > 30) {
                [Utility deleteAndRefreshDates];
                [[NSUserDefaults standardUserDefaults]setObject:[NSDate date] forKey:@"lastRefreshedDate"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
        else{
            [Utility deleteAndRefreshDates];
            [[NSUserDefaults standardUserDefaults]setObject:[NSDate date] forKey:@"lastRefreshedDate"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }

    }

    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;

    [Fabric with:@[[Crashlytics class]]];
    
    //USED FOR DELETING THE SERVER DELETED DATES
    [Utility getCurrentDatesIdsAndResponseBlock:^(id object, BOOL status, NSError *error) {}];
    [Utility hebrewDateTest];
    [[TestSQB alloc] printCurrentNotifications];
    return YES;
}
-(void)showAlert {
    UIAlertController *objAlertController = [UIAlertController alertControllerWithTitle:@"Alert" message:@"show an alert!" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK"
                                                           style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                                                               NSLog(@"Ok clicked!");
                                                           }];
    
    [objAlertController addAction:cancelAction];
    
    
    [[[[[UIApplication sharedApplication] windows] objectAtIndex:0] rootViewController] presentViewController:objAlertController animated:YES completion:^{
    }];
    
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    
    if ([[self topViewController] isKindOfClass:[ViewController class]]) {
        ViewController *vc = (ViewController *)[self topViewController];
        [vc.calendarViewMain setSelectedDate:[NSDate date]];
    }
    
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"Yahrtzeits_of_Gedolim"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}


- (NSManagedObjectContext *)managedObjectContext{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Yahrtzeits_of_Gedolim" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"DatabaseModel.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}



- (UIViewController *)topViewController{
    return [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController *)topViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil) {
        return rootViewController;
    }
    
    if ([rootViewController.presentedViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        return [self topViewController:lastViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    return [self topViewController:presentedViewController];
}

- (UIViewController *)getVisibleViewController {
    UIViewController *topViewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    while (topViewController.presentedViewController) topViewController = topViewController.presentedViewController;
    
    return topViewController;
}





- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler{
    [UIApplication sharedApplication].applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;
}

@end
