//
//  ViewController.h
//  Yahrtzeits of Gedolim
//
//  Created by iBuildX on 30/05/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Daysquare.h"
#import <MessageUI/MessageUI.h>
#import "DAYCalendarView.h"


@interface ViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate , DateDelegate , UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *grMonthTxt;
@property (weak, nonatomic) IBOutlet UILabel *grYearText;

@property (weak, nonatomic) IBOutlet UILabel *hbMonthTxt;
@property (weak, nonatomic) IBOutlet UILabel *hbYearTxt;



@property (weak, nonatomic) IBOutlet UIButton *notificationBtn;


@property (weak, nonatomic) IBOutlet UIView *tableViewSuperView;



@property (weak, nonatomic) IBOutlet UITableView *subjectsCalendar;
    
@property (weak, nonatomic) IBOutlet DAYCalendarView *calendarViewMain;
    

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pickerViewTopSpace;
@property (weak, nonatomic) IBOutlet UIView *pickerSuperView;


@property (weak, nonatomic) IBOutlet UIPickerView *hebrewDayPicker;
@property (weak, nonatomic) IBOutlet UIPickerView *hebrewMonthPicker;
@property (weak, nonatomic) IBOutlet UIPickerView *hebrewYearPicker;


@property BOOL isAfterDelete;


    
- (IBAction)contactSponsorAction:(UIButton *)sender;
- (IBAction)notificationBtnAction:(UIButton *)sender;
- (IBAction)previousMonthAction:(UIButton *)sender;
- (IBAction)nextMonthAction:(UIButton *)sender;


- (IBAction)pickerViewShowAction:(UIButton *)sender;
- (IBAction)pickerViewSetAction:(UIButton *)sender;
- (IBAction)pickerViewCancelAction:(UIButton *)sender;
- (IBAction)todayAction:(UIButton *)sender;


- (IBAction)suggestAction:(UIButton *)sender;
- (IBAction)searchByNameAction:(UIButton *)sender;









@end

