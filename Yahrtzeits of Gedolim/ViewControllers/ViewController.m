//
//  ViewController.m
//  Yahrtzeits of Gedolim
//
//  Created by iBuildX on 30/05/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import "ViewController.h"
#import "CalendarSubjectsTableViewCell.h"
#import "Yahrtzeits_of_Gedolim-Swift.h"
#import "Utility.h"
#import "SubjectModel.h"
#import "DetailsViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "DAYUtils.h"
#import <SwiftSpinner/SwiftSpinner-Swift.h>
#import <AMSmoothAlert/AMSmoothAlertView.h>

@interface ViewController (){
    SubjectDateModel *selectDateSubjects;
    MBProgressHUD *hud;
    
    NSMutableArray *hebrewDays;
    NSMutableArray *hebrewMonths;
    NSMutableArray *hebrewYears;
    
    
    BOOL isFirstTime;
}

@end

@implementation ViewController
#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    isFirstTime = YES;
    // Do any additional setup after loading the view, typically from a nib.
    
    hebrewDays = [[NSMutableArray alloc] init];
    for (int i = 1; i < 31; i++) {
        [hebrewDays addObject:[NSString stringWithFormat:@"%i" , i]];
    }
    
    
    hebrewYears = [[NSMutableArray alloc] init];
    for (int i = 5700; i < 6000; i++) {
        [hebrewYears addObject:[NSString stringWithFormat:@"%i" , i]];
    }
    
    
    hebrewMonths = [[NSMutableArray alloc] initWithObjects:@"Nisan",
                    @"Iyyar" ,
                    @"Sivan",
                    @"Tamuz",
                    @"Av",
                    @"Elul",
                    @"Tishrei",
                    @"Cheshvan",
                    @"Kislev",
                    @"Tevet",
                    @"Sh'vat",
                    @"Adar",
                    @"Adar II", nil];

    
    
    self.subjectsCalendar.delegate = self;
    self.subjectsCalendar.dataSource = self;

    self.calendarViewMain.delegate = self;
    [self.calendarViewMain setSelectedDate:[NSDate date]];
    
    
    self.hebrewDayPicker.delegate = self;
    self.hebrewDayPicker.dataSource = self;

    self.hebrewMonthPicker.delegate = self;
    self.hebrewMonthPicker.dataSource = self;

    self.hebrewYearPicker.delegate = self;
    self.hebrewYearPicker.dataSource = self;

    
    [self.view layoutIfNeeded];
    self.pickerViewTopSpace.constant = self.pickerSuperView.frame.size.width;
    
    
    self.isAfterDelete  = NO;


}
-(void)viewWillAppear:(BOOL)animated{
    [self handleCalenderView ];
}
-(void)viewDidAppear:(BOOL)animated{
    self.pickerViewTopSpace.constant = self.pickerSuperView.frame.size.width;
    
    
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    BOOL isNotifications = [preferences boolForKey:@"gedolNotifications"];
    
    if (isNotifications) {
        //[self.notificationBtn setImage:[UIImage imageNamed:@"notificationBtn"] forState:UIControlStateNormal];
        self.notificationBtn.tag = 1;
    }
    else{
        //[self.notificationBtn  setImage:[UIImage imageNamed:@"notificationBtnOff"] forState:UIControlStateNormal];
        self.notificationBtn.tag = 0;
    }
    
    
    
    [[UNUserNotificationCenter currentNotificationCenter]getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
        
        switch (settings.authorizationStatus) {
            case UNAuthorizationStatusNotDetermined:{
                NSLog(@"UNAuthorizationStatusNotDetermined");
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[self.notificationBtn  setImage:[UIImage imageNamed:@"notificationBtnOff"] forState:UIControlStateNormal];
                    self.notificationBtn.tag = 0;
                });
                break;
            }
            case UNAuthorizationStatusDenied:{
                NSLog(@"UNAuthorizationStatusDenied");
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.notificationBtn.tag = 0;
                    //[SANotificationView showSAStatusBarBannerWithMessage:@"Authorization Status Denied" backgroundColor:[UIColor redColor] textColor:[UIColor whiteColor  ] showTime:3000.0];
                    if(isFirstTime == YES){
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Your Notifications are Turned Off!" message:@"You did not grant the app access to receive daily notifications.  You can change this at any time within your device “Settings”." preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                        [alert addAction:ok];
                        [self presentViewController:alert animated:true completion:nil];
                        isFirstTime = NO;
                    }
                    

                });
                break;
            }
            case UNAuthorizationStatusAuthorized:{
                NSLog(@"UNAuthorizationStatusAuthorized");
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[self.notificationBtn  setImage:[UIImage imageNamed:@"notificationBtn"] forState:UIControlStateNormal];
                    self.notificationBtn.tag = 1;
                    
                    [SANotificationView removeStatusView];
                });
                break;
            }
            default:
                break;
        }
    }];

    
    if (self.isAfterDelete == YES) {
        [self.calendarViewMain setSelectedDate:[NSDate date]];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table View Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return selectDateSubjects.Dates.count;
}
    
   
    
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CalendarSubjectsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CalendarSubjectsTableViewCell"];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd"];
    cell.grDauText.text = [df stringFromDate:self.calendarViewMain.selectedDate];
    if (self.calendarViewMain.selectedHebrewDate) {
        NSArray *dateComponents = [self.calendarViewMain.selectedHebrewDate.hebrew componentsSeparatedByString:@" "];
        cell.hbDayText.text = [NSString stringWithFormat:@"%@",[dateComponents firstObject]];
    }
    SubjectModel *subject = [selectDateSubjects.Dates objectAtIndex:indexPath.row];
    
    NSString * const kAPIDateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSSSSS";
    
    // convert API date string
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:kAPIDateFormat];
    NSDate *subjectDate =  [formatter dateFromString:[subject.created_date stringByReplacingOccurrencesOfString:@"Z" withString:@""]];
    
    NSLog(@"Date = %@" , subjectDate);
    
    cell.subjectTitleText.text = subject.subjectTitle;
    cell.subjectSubTitleText.text = subject.subjectTitle;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    SubjectModel *subject = [selectDateSubjects.Dates objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"ToDetials" sender:subject];
}

#pragma mark - View Buttons Actions

- (IBAction)contactSponsorAction:(UIButton *)sender {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController * mail = [[MFMailComposeViewController alloc]init];
        mail.mailComposeDelegate = self;
        [mail setMessageBody:@"<p>Hi,</p><p>I would like to dedicate a Yartzeit of a Gadol, please get back to me.</p><p>Thank you.</p>" isHTML:YES];
        [mail setSubject:@"Dedicate a Gadol"];
        [mail setToRecipients:@[@"info@yartzeits.com"]];
        [self presentViewController:mail animated:YES completion:NULL];
    }
    else if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[@"mailto://" stringByAppendingString:@"info@yartzeits.com"]]]){
        NSString* to = @"info@yartzeits.com";
        NSString *encodedSubject = [NSString stringWithFormat:@"SUBJECT=%@", @"Dedicate a Gadol"];
        NSString *encodedBody = [NSString stringWithFormat:@"BODY=%@", @"<p>Hi,</p><p>I would like to dedicate a Yartzeit of a Gadol, please get back to me.</p><p>Thank you.</p>"];
        NSString *encodedTo = [to stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodedURLString = [NSString stringWithFormat:@"mailto:%@?%@&%@", encodedTo, encodedSubject, encodedBody];
        NSURL *mailtoURL = [NSURL URLWithString:encodedURLString];
        [[UIApplication sharedApplication] openURL:mailtoURL options:@{} completionHandler:nil];
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Cannot send email" message:@"Email is not available. Please check your settings." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:true completion:nil];
    }

    
}

- (IBAction)notificationBtnAction:(UIButton *)sender {
    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initFadeAlertWithTitle:@"Enter a Yartzeit" andText:@"Your entry will appear only on this device." andCancelButton:NO forAlertType:AlertInfo andColor:[UIColor colorWithRed:(97.0/255.0) green:(75.0/255.0) blue:(234.0/255.0) alpha:1.0] withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
        [alert dismissAlertView];
        [self performSegueWithIdentifier:@"ToAdd" sender:self];
    }];
    
    
    alert.titleFont = [UIFont fontWithName:@"HelveticaNeue-BoldItalic" size:22.0f];
    alert.textFont = [UIFont fontWithName:@"HelveticaNeue" size:16.0f];
    alert.logoView.image = [UIImage imageNamed:@"candle"];
    alert.logoView.contentMode = UIViewContentModeScaleAspectFill;
    
    [alert.defaultButton setTitle:@"OK" forState:UIControlStateNormal];
    alert.defaultButton.layer.cornerRadius = 10.0f;
    alert.cornerRadius = 20.0f;
    [alert show];
    NSMutableArray *yourArray = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"NotifiedDates"] mutableCopy];
    yourArray = [[yourArray sortedArrayUsingComparator:^NSComparisonResult(NSDate *obj1, NSDate *obj2) {
        return [obj1 compare:obj2];
    }] mutableCopy];
    
    NSLog(@"yourArray COUNT = %lu" , (unsigned long)yourArray.count);

    for (NSDate *date in yourArray) {
        NSLog(@"NOTIFICATION DATE = %@" , date);
    }
    
}



- (IBAction)previousMonthAction:(UIButton *)sender {
    [self.calendarViewMain jumpToPreviousMonth];
    [sender setEnabled:NO];
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [sender setEnabled:YES];
    });
}

- (IBAction)nextMonthAction:(UIButton *)sender {
    [self.calendarViewMain jumpToNextMonth];
    [sender setEnabled:NO];
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [sender setEnabled:YES];
    });
}

- (IBAction)pickerViewShowAction:(UIButton *)sender {
    if (self.pickerViewTopSpace.constant == 0) {
        self.pickerViewTopSpace.constant = self.pickerSuperView.frame.size.width;
    }
    else{
        self.pickerViewTopSpace.constant = 0;
    }
    
    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self.view layoutIfNeeded];
    } completion:nil];
}

- (IBAction)pickerViewSetAction:(UIButton *)sender {
    NSString *year = [hebrewYears objectAtIndex:[self.hebrewYearPicker selectedRowInComponent:0]];
    NSString *month = [hebrewMonths objectAtIndex:[self.hebrewMonthPicker selectedRowInComponent:0]];
    NSString *day = [hebrewDays objectAtIndex:[self.hebrewDayPicker selectedRowInComponent:0]];

    
    NSLog(@"SELCTED DAY = %@" , [hebrewDays objectAtIndex:[self.hebrewDayPicker selectedRowInComponent:0]]);
    NSLog(@"SELCTED MONTH = %@" , [hebrewMonths objectAtIndex:[self.hebrewMonthPicker selectedRowInComponent:0]]);
    NSLog(@"SELCTED YEAR = %@" , [hebrewYears objectAtIndex:[self.hebrewYearPicker selectedRowInComponent:0]]);
    
    if ([month isEqualToString:@"Adar II"]) {
        month = @"Adar%20II";
    }
    
    NSString *linkString = [NSString stringWithFormat:@"http://www.hebcal.com/converter/?cfg=json&hy=%@&hm=%@&hd=%@&h2g=1", year, month, day];
    self.hbMonthTxt.text = month;
    self.hbYearTxt.text =  year;
    dispatch_async(dispatch_get_main_queue(), ^{
        [SwiftSpinner show:@"Converting Date..." animated:YES];
    });
    [Utility getHebrewDateFromLink:linkString AndResponseBlock:^(id object, BOOL status, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SwiftSpinner hide:nil];
        });
        if (status) {
            NSError *error;
            DateModel *date = [[DateModel alloc] initWithDictionary:object error:&error];
            if (error) {
                NSLog(@"ERROR ON JSONMODEL = %@" , error.description);
            }
            else{
                
                [self.calendarViewMain jumpToMonth:[date.gm integerValue] year:[date.gy integerValue] day:[date.gd integerValue]];
                
                
                
                self.hbMonthTxt.text = date.hm;
                self.hbYearTxt.text = [date.hy stringValue];
                self.calendarViewMain.selectedHebrewDate = date;
                [self getSubjectsOnDate:date];
                
                [self.hebrewDayPicker selectRow:[hebrewDays indexOfObject:[NSString stringWithFormat:@"%@" , date.hd]] inComponent:0 animated:YES];
                [self.hebrewYearPicker selectRow:[hebrewYears indexOfObject:[NSString stringWithFormat:@"%@" , date.hy]] inComponent:0 animated:YES];
                NSString *hebrewMonth =date.hm;
                if([hebrewMonth isEqualToString:@"Adar I"]){
                    hebrewMonth = @"Adar";
                }
                [self.hebrewMonthPicker selectRow:[hebrewMonths indexOfObject:hebrewMonth] inComponent:0 animated:YES];
                
            }
        }
        else{
            NSLog(@"ERROR ON RESPONSE = %@" , error.description);
            dispatch_async(dispatch_get_main_queue(), ^{
                [SANotificationView showSAStatusBarBannerWithMessage:@"Date NOT Found" backgroundColor:[UIColor redColor] textColor:[UIColor whiteColor  ] showTime:3.0];
            });
        }
    }];

    
    
    
    
    
    
    
    [self pickerViewShowAction:nil];
}

- (IBAction)pickerViewCancelAction:(UIButton *)sender {
    [self pickerViewShowAction:nil];
}

- (IBAction)todayAction:(UIButton *)sender {
    [self.calendarViewMain setSelectedDate:[NSDate date]];
}

- (IBAction)suggestAction:(UIButton *)sender {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController * mail = [[MFMailComposeViewController alloc]init];
        mail.mailComposeDelegate = self;
        [mail setSubject:@"suggestions Yartzeits app"];
        [mail setToRecipients:@[@"info@yartzeits.com"]];
        [self presentViewController:mail animated:YES completion:NULL];
    }
    else if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[@"mailto://" stringByAppendingString:@"info@yartzeits.com"]]]){
        NSString* to = @"info@yartzeits.com";
        NSString *encodedSubject = [NSString stringWithFormat:@"SUBJECT=%@", @"suggestions Yartzeits app"];
        NSString *encodedTo = [to stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodedURLString = [NSString stringWithFormat:@"mailto:%@?%@", encodedTo, encodedSubject];
        NSURL *mailtoURL = [NSURL URLWithString:encodedURLString];
        [[UIApplication sharedApplication] openURL:mailtoURL options:@{} completionHandler:nil];
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Cannot send email" message:@"Email is not available. Please check your settings." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:true completion:nil];
    }
}

- (IBAction)searchByNameAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"ToNames" sender:self];
}


#pragma mark - Calendar Delegate

- (void) didSelectData: (NSDate *) date andHebrewDate:(DateModel *)hebrewDate{
    //[self.datePickerView setDate:date animated:YES];
    
    [self.subjectsCalendar reloadData];
    NSLog(@"SELECTED DATE = %@" , date);
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MMM"];
    self.grMonthTxt.text = [df stringFromDate:date];
    [df setDateFormat:@"yyyy"];
    self.grYearText.text = [df stringFromDate:date];
    
    
    
    if (hebrewDate) {
        self.hbMonthTxt.text = hebrewDate.hm;
        self.hbYearTxt.text = [hebrewDate.hy stringValue];
        [self getSubjectsOnDate:hebrewDate];
        
        
        [self.hebrewDayPicker selectRow:[hebrewDays indexOfObject:[NSString stringWithFormat:@"%@" , hebrewDate.hd]] inComponent:0 animated:YES];
        [self.hebrewYearPicker selectRow:[hebrewYears indexOfObject:[NSString stringWithFormat:@"%@" , hebrewDate.hy]] inComponent:0 animated:YES];
        NSString *hebrewMonth =hebrewDate.hm;
        if([hebrewMonth isEqualToString:@"Adar I"]){
            hebrewMonth = @"Adar";
        }
        [self.hebrewMonthPicker selectRow:[hebrewMonths indexOfObject:hebrewMonth] inComponent:0 animated:YES];

    }
    else{
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSInteger year = [calendar component:NSCalendarUnitYear fromDate:date];
        NSInteger month = [calendar component:NSCalendarUnitMonth fromDate:date];
        NSInteger day = [calendar component:NSCalendarUnitDay fromDate:date];
        
        NSString *linkString = [NSString stringWithFormat:@"http://www.hebcal.com/converter/?cfg=json&gy=%li&gm=%li&gd=%li&g2h=1", (long)year, (long)month, (long)day];
        self.hbMonthTxt.text =  @"...";
        self.hbYearTxt.text =  @"...";
        [Utility getHebrewDateFromLink:linkString AndResponseBlock:^(id object, BOOL status, NSError *error) {
            if (status) {
                NSError *error;
                DateModel *date = [[DateModel alloc] initWithDictionary:object error:&error];
                if (error) {
                    NSLog(@"ERROR ON JSONMODEL = %@" , error.description);
                }
                else{
                    self.hbMonthTxt.text = date.hm;
                    self.hbYearTxt.text = [date.hy stringValue];
                    self.calendarViewMain.selectedHebrewDate = date;
                    [self getSubjectsOnDate:date];

                    [self.hebrewDayPicker selectRow:[hebrewDays indexOfObject:[NSString stringWithFormat:@"%@" , date.hd]] inComponent:0 animated:YES];
                    [self.hebrewYearPicker selectRow:[hebrewYears indexOfObject:[NSString stringWithFormat:@"%@" , date.hy]] inComponent:0 animated:YES];
                    NSString *hebrewMonth =date.hm;
                    if([hebrewMonth isEqualToString:@"Adar I"]){
                        hebrewMonth = @"Adar";
                    }
                    [self.hebrewMonthPicker selectRow:[hebrewMonths indexOfObject:hebrewMonth] inComponent:0 animated:YES];
                }
            }
            else{
                NSLog(@"ERROR ON RESPONSE = %@" , error.description);
            }
        }];
    }
    
}

#pragma mark - Helper
-(void)getSubjectsOnDate :(DateModel *)model{
    NSDictionary *monthsDict = @{@"Nisan" : @"1",
                                 @"Iyyar" : @"2",
                                 @"Sivan" : @"3",
                                 @"Tamuz" : @"4",
                                 @"Av" : @"5",
                                 @"Elul" : @"6",
                                 @"Tishrei" : @"7",
                                 @"Cheshvan" : @"8",
                                 @"Kislev" : @"9",
                                 @"Tevet" : @"10",
                                 @"Sh'vat" : @"11",
                                 @"Adar I" : @"12",
                                 @"Adar II": @"13",
                                 @"Adar": @"12"
                                 };
    NSLog(@"MONTH = %@" , model.hm);
    NSDictionary *dict = @{ @"month" : monthsDict[model.hm] , @"day" : [model.hd stringValue]};
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (selectDateSubjects.Dates.count) {
            selectDateSubjects.Dates = [[NSArray<SubjectModel> alloc] init];
        }
        [self.subjectsCalendar reloadData];
        //hud = [MBProgressHUD showHUDAddedTo:self.tableViewSuperView animated:YES];
        //hud.mode = MBProgressHUDModeIndeterminate;
        //hud.label.text = @"Loading...";
        [SwiftSpinner show:@"Loading..." animated:YES];
        
    });
    [Utility getSubjectsForDateWithDictionary:dict AndResponseBlock:^(id object, BOOL status, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            //[MBProgressHUD hideHUDForView:self.tableViewSuperView animated:YES];
            [SwiftSpinner hide:nil];
        });
        if (status) {
            NSLog(@"RETURN FROM PYTHON = %@" , object);
            NSError *errorDecode;
            selectDateSubjects = [[SubjectDateModel alloc] initWithDictionary:object error:&errorDecode];
            if (errorDecode) {
                NSLog(@"ERROR ON PYTHON DECODE = %@", errorDecode.description);
            }
            
            
            
             
             NSError *errorCoreData;
             
             NSFetchRequest *fetchRequest = [Date fetchRequest];
             NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"notificationId contains[cd] %@", @"-Gedol-Manual"];
             NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"day == %i",[model.hd intValue]];
             NSPredicate *predicate3 =  [NSPredicate predicateWithFormat:@"month == %i",[monthsDict[model.hm] intValue]];
             NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2 , predicate3]];
             fetchRequest.predicate = predicate;
             NSArray *datesAddedManually = [[Utility managedObjectContext] executeFetchRequest:fetchRequest error:&errorCoreData] ;
             
             if (errorCoreData) {
             NSLog(@"ERROR IN notificationId == %@" , errorCoreData.description);
             }
            
            if (datesAddedManually.count) {
                for (Date* date in datesAddedManually) {
                    
                    SubjectModel *newSubject = [[SubjectModel alloc] init];
                    newSubject.subjectTitle = date.subjectTitle;
                    newSubject.subjectDescription = date.subjectDescription;
                    [selectDateSubjects.Dates addObject:newSubject];
                }

            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.subjectsCalendar reloadData];
            });
            
        }
        else{
            NSLog(@"ERROR ON PYTHON = %@", error.description);
        }
    }];
}

-(void)handleCalenderView{
    
    UIColor *componentTextColor = [UIColor whiteColor];
    self.calendarViewMain.weekdayHeaderTextColor = [UIColor whiteColor];
    self.calendarViewMain.weekdayHeaderWeekendTextColor = [UIColor whiteColor];
    self.calendarViewMain.componentTextColor = componentTextColor;
    self.calendarViewMain.todayIndicatorColor = [UIColor clearColor];
    self.calendarViewMain.selectedIndicatorColor = [UIColor whiteColor];
    self.calendarViewMain.highlightedComponentTextColor = [UIColor redColor];
    

    
    //self.calendarViewMain.showUserEvents = YES;
    
    self.calendarViewMain.indicatorRadius = 20;
    [self.calendarViewMain reloadViewAnimated:YES];
    
}

-(void)fireNotification{
    return;
    UNMutableNotificationContent *objNotificationContent = [[UNMutableNotificationContent alloc] init];
    
    objNotificationContent.title = [NSString localizedUserNotificationStringForKey:@"Notification!" arguments:nil];
    
    objNotificationContent.body = [NSString localizedUserNotificationStringForKey:@"This is local notification message!"arguments:nil];
    
    objNotificationContent.sound = [UNNotificationSound defaultSound];
    
    // Deliver the notification in five seconds.
    UNTimeIntervalNotificationTrigger *trigger =  [UNTimeIntervalNotificationTrigger                                             triggerWithTimeInterval:3.f repeats:NO];
    
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"ten"                                                                            content:objNotificationContent trigger:trigger];
    
    // 3. schedule localNotification
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    
    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        if (!error) {
            NSLog(@"Local Notification succeeded");
        } else {
            NSLog(@"Local Notification failed");
        }
    }];
}
#pragma mark - Mail Delegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    [controller dismissViewControllerAnimated:YES completion:nil];
    if (result == MessageComposeResultCancelled)
        NSLog(@"Message cancelled");
    else if (result == MessageComposeResultSent){
        NSLog(@"Message sent");
    }
    else
        NSLog(@"Message failed");
}

#pragma mark - PickerView Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView.tag == 1) {
        return hebrewDays.count;
    }
    else if (pickerView.tag == 2){
        return hebrewMonths.count;
    }
    else if (pickerView.tag == 3){
        return hebrewYears.count;
    }
    return 0;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(nullable UIView *)view {
    
    UILabel* tView = (UILabel*)view;
    if (!tView)
    {
        tView = [[UILabel alloc] init];
        [tView setFont:[UIFont fontWithName:self.hbYearTxt.font.fontName size:16.0]];
        [tView setTextAlignment:NSTextAlignmentCenter];
        tView.numberOfLines=1;
        tView.textColor = [UIColor darkTextColor];
    }
    tView.text = @"";
    if (pickerView.tag == 1) {
        tView.text = [hebrewDays objectAtIndex:row];
    }
    else if (pickerView.tag == 2){
        tView.text = [hebrewMonths objectAtIndex:row];
    }
    else if (pickerView.tag == 3){
        tView.text = [hebrewYears objectAtIndex:row];
    }
    return tView;
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.destinationViewController isKindOfClass:[DetailsViewController class]]) {
        DetailsViewController *vc = segue.destinationViewController;
        vc.selectedDate = self.calendarViewMain.selectedDate;
        vc.selectedHebrewDate = self.calendarViewMain.selectedHebrewDate;
        vc.selectedSubjectModel = (SubjectModel *)sender;
        
    }
    
    
}

#pragma mark - Utility

-(UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

@end
