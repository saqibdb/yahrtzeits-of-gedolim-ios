//
//  AddSubjectViewController.m
//  Yahrtzeits of Gedolim
//
//  Created by iBuildX on 19/08/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import "AddSubjectViewController.h"
#import <SwiftSpinner/SwiftSpinner-Swift.h>
#import "CustomDatePickerView.h"
#import "Utility.h"
#import "DateModel.h"
#import "AMSmoothAlertView.h"
#import "HebrewDateConvertorViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"


@interface AddSubjectViewController (){
    NSMutableArray *hebrewDays;
    NSMutableArray *hebrewMonths;
    
    NSMutableArray *uploadedImagesUrls;
    
    
    BOOL isFirstTime;

}

@end

@implementation AddSubjectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isFirstTime = YES;
    
    
    // Do any additional setup after loading the view.
    hebrewDays = [[NSMutableArray alloc] init];
    for (int i = 1; i < 31; i++) {
        [hebrewDays addObject:[NSString stringWithFormat:@"%i" , i]];
    }
    
    uploadedImagesUrls = [[NSMutableArray alloc] init];
   
    
    hebrewMonths = [[NSMutableArray alloc] initWithObjects:@"Nisan",
                    @"Iyyar" ,
                    @"Sivan",
                    @"Tamuz",
                    @"Av",
                    @"Elul",
                    @"Tishrei",
                    @"Cheshvan",
                    @"Kislev",
                    @"Tevet",
                    @"Sh'vat",
                    @"Adar",
                    @"Adar II", nil];
    
    self.hebrewDayPicker.delegate = self;
    self.hebrewDayPicker.dataSource = self;
    
    self.hebrewMonthPicker.delegate = self;
    self.hebrewMonthPicker.dataSource = self;
    
    
    self.nMainTV.text = @"";
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    gestureRecognizer.cancelsTouchesInView = NO; //so that action such as clear text field button can be pressed
    [self.view addGestureRecognizer:gestureRecognizer];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewDidAppear:(BOOL)animated{
    if(isFirstTime == YES){
        [self.hebrewDayPicker selectRow:5 inComponent:0 animated:YES];
        [self.hebrewMonthPicker selectRow:5 inComponent:0 animated:YES];
        isFirstTime = NO;
    }
    

}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - keyboard movements
- (void)keyboardWillShow:(NSNotification *)notification{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    self.toolbarBottomConstraint.constant = keyboardSize.height;
    NSLog(@"KEYBOARD HEIGHT = %f" , keyboardSize.height);
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification{
    self.toolbarBottomConstraint.constant = -44.0;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void) hideKeyboard {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - PickerView Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView.tag == 1) {
        return hebrewDays.count;
    }
    else if (pickerView.tag == 2){
        return hebrewMonths.count;
    }
    return 0;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(nullable UIView *)view {
    
    UILabel* tView = (UILabel*)view;
    if (!tView)
    {
        tView = [[UILabel alloc] init];
        [tView setFont:[UIFont fontWithName:@"" size:16.0]];
        [tView setTextAlignment:NSTextAlignmentCenter];
        tView.numberOfLines=1;
        tView.textColor = [UIColor whiteColor];
    }
    tView.text = @"";
    if (pickerView.tag == 1) {
        tView.text = [hebrewDays objectAtIndex:row];
    }
    else if (pickerView.tag == 2){
        tView.text = [hebrewMonths objectAtIndex:row];
    }
    return tView;
    
}

- (IBAction)closeAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)convertAction:(UIButton *)sender {
    NSDictionary *monthsDict = @{@"Nisan" : @"1",
                                 @"Iyyar" : @"2",
                                 @"Sivan" : @"3",
                                 @"Tamuz" : @"4",
                                 @"Av" : @"5",
                                 @"Elul" : @"6",
                                 @"Tishrei" : @"7",
                                 @"Cheshvan" : @"8",
                                 @"Kislev" : @"9",
                                 @"Tevet" : @"10",
                                 @"Sh'vat" : @"11",
                                 @"Adar I" : @"12",
                                 @"Adar II": @"13",
                                 @"Adar": @"12"
                                 };
    RMAction *selectAction = [RMAction<CustomDatePickerView *> actionWithTitle:@"Select" style:RMActionStyleDone andHandler:^(RMActionController<CustomDatePickerView *> *controller) {
        NSLog(@"Successfully selected date: %@", controller.contentView.mainDatePicker.date);
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSInteger year = [calendar component:NSCalendarUnitYear fromDate:controller.contentView.mainDatePicker.date];
        NSInteger month = [calendar component:NSCalendarUnitMonth fromDate:controller.contentView.mainDatePicker.date];
        NSInteger day = [calendar component:NSCalendarUnitDay fromDate:controller.contentView.mainDatePicker.date];
        
        
        
        NSString *linkString = [NSString stringWithFormat:@"http://www.hebcal.com/converter/?cfg=json&gy=%li&gm=%li&gd=%li&g2h=1", (long)year, (long)month, (long)day];
        if (controller.contentView.afterSunsetSwitch.isOn) {
            NSLog(@"After Sunset is on");
            linkString = [NSString stringWithFormat:@"http://www.hebcal.com/converter/?cfg=json&gy=%li&gm=%li&gd=%li&gs=on&g2h=1", (long)year, (long)month, (long)day];
        }
        
        
        [SwiftSpinner show:@"Converting..." animated:YES];

        [Utility getHebrewDateFromLink:linkString AndResponseBlock:^(id object, BOOL status, NSError *error) {
            [SwiftSpinner hide:nil];
            if (status) {
                NSError *error;
                DateModel *date = [[DateModel alloc] initWithDictionary:object error:&error];
                if (error) {
                    NSLog(@"ERROR ON JSONMODEL = %@" , error.description);
                }
                else{
                    [self.hebrewDayPicker selectRow:([date.hd intValue] - 1) inComponent:0 animated:YES];
                    [self.hebrewMonthPicker selectRow:([monthsDict[date.hm] intValue] - 1) inComponent:0 animated:YES];

                }
            }
            else{
                NSLog(@"ERROR ON RESPONSE = %@" , error.description);
            }
        }];
        
        
    }];
    
    RMAction<UIDatePicker *> *cancelAction = [RMAction<UIDatePicker *> actionWithTitle:@"Cancel" style:RMActionStyleCancel andHandler:^(RMActionController<UIDatePicker *> *controller) {
        NSLog(@"Date selection was canceled");
    }];
    
    

    
    
    HebrewDateConvertorViewController *dateSelectionController = [HebrewDateConvertorViewController actionControllerWithStyle:RMActionControllerStyleWhite title:@"Convert" message:@"Enter Gregorian date to convert to Hebrew Date." selectAction:selectAction andCancelAction:cancelAction];
    //dateSelectionController.datePicker.datePickerMode = UIDatePickerModeDate;
    [self presentViewController:dateSelectionController animated:YES completion:nil];
    
    
    
    
}

- (IBAction)addAction:(UIButton *)sender {
    if (self.titleTxt.text.length == 0 || self.nMainTV.text.length == 0) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Incomplete" andText:@"Please Fill all Fields before adding." andCancelButton:NO forAlertType:AlertInfo];
        alert.animationType = DropAnimation;
        alert.logoView.image = [UIImage imageNamed:@"candle"];
        [alert show];
        
        NSLog(@"TEXT is %@" , self.nMainTV.text);
        if([self.nMainTV.text containsString:@"\n"]){
            NSLog(@"CONTAINS LINE BREAK");
        }
        
        return;
    }
    
    
    
    
    NSCalendar * hebrew = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierHebrew];
    NSDateFormatter * formatterHebrew = [[NSDateFormatter alloc] init];
    [formatterHebrew setDateStyle:NSDateFormatterLongStyle];
    [formatterHebrew setTimeStyle:NSDateFormatterNoStyle];
    [formatterHebrew setCalendar:hebrew];
    [formatterHebrew setDateFormat:@"yyyy-MMM-dd"];
    int hebrewYear = [[formatterHebrew stringFromDate:[NSDate date]] intValue];
    
    NSDictionary *monthsDict = @{@1 : @"Nisan",
                                 @2 : @"Iyar" ,
                                 @3 : @"Sivan",
                                 @4 : @"Tamuz",
                                 @5 : @"Av",
                                 @6 : @"Elul",
                                 @7 : @"Tishri",
                                 @8 : @"Heshvan",
                                 @9 : @"Kislev",
                                 @10 : @"Tevet",
                                 @11 : @"Shevat",
                                 @12 : @"Adar I",
                                 @13 : @"Adar II"
                                 };
    
    NSString *hebrewMonthName = monthsDict[@(([self.hebrewMonthPicker selectedRowInComponent:0] + 1))] ;
    NSString *hebrewYearStr = [NSString stringWithFormat:@"%i-%@-%li",hebrewYear, hebrewMonthName, ([self.hebrewDayPicker selectedRowInComponent:0] + 1)];
    NSDate *convertedDate = [Utility convert:hebrewYearStr];

    

    Date *newDate = [[Date alloc] initWithContext:[Utility managedObjectContext]];
    
    int r = rand() % 474;
    
    NSFetchRequest *fetchRequest = [Date fetchRequest];
    NSArray *maxDates = [[Utility managedObjectContext] executeFetchRequest:fetchRequest error:nil];
    
    NSArray *maxIdArray = [maxDates sortedArrayUsingComparator:^NSComparisonResult(Date *date1, Date *date2) {
        return date1.id < date2.id;
    }];
    
    if(maxIdArray.count > 0) {
        Date *maxDate = [maxIdArray firstObject];
        NSLog(@"MAX ID IS %i" , maxDate.id);
        r = maxDate.id+1;
    }
    
    NSString *notificationStr = [NSString stringWithFormat:@"%i-Gedol-Manual",r];
    NSLog(@"notificationStr = %@" , notificationStr);
    newDate.id = r;

    newDate.day = ([self.hebrewDayPicker selectedRowInComponent:0] + 1);
    newDate.month = ([self.hebrewMonthPicker selectedRowInComponent:0] + 1);
    newDate.subjectTitle = self.titleTxt.text;
    //
    
    NSString *bodyText = [self.nMainTV.text stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
    
    NSString *descriptionText = [NSString stringWithFormat:@"<h1><span style=\"color: #ffffff;\">%@</span></h1>\r\n<h2><span style=\"color: #ffffff;\">%@</span></h2>", self.titleTxt.text, bodyText];
    
    
    for(NSString *urlStr in uploadedImagesUrls){
        
        NSRange rOriginal = [descriptionText rangeOfString:@"*"];
        
        if (NSNotFound != rOriginal.location) {
            descriptionText = [descriptionText stringByReplacingCharactersInRange:rOriginal withString:urlStr];
        }
    }
    
    
    
    newDate.subjectDescription = descriptionText;

    
    
    
    
    
    //newDate.attributedDescription = self.nMainTV.attributedText;
    NSString * const kAPIDateFormat = @"yyyy-MM-dd HH:mm:ss";//2018-08-10 17:08:40
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:kAPIDateFormat];
    NSString *createdDateStr = @"1970-01-01 01:00:00";
    NSString *modifiedDateStr = @"1970-01-01 01:00:00";
    if (createdDateStr.length > 19) {
        createdDateStr = [createdDateStr substringToIndex:18];
        createdDateStr = [createdDateStr stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    }
    if (modifiedDateStr.length > 19) {
        modifiedDateStr = [modifiedDateStr substringToIndex:18];
        modifiedDateStr = [modifiedDateStr stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    }
    newDate.created_date =  [formatter dateFromString:createdDateStr];
    newDate.modified_date =  [NSDate dateWithTimeInterval:1.0 sinceDate:[formatter dateFromString:modifiedDateStr]];
    newDate.notificationId = notificationStr;
    newDate.grDate = convertedDate;
    [Utility fireNotificationForDates:convertedDate andTitles:self.titleTxt.text andId:notificationStr andIndex:0 andAllDatesDicts:[[NSMutableArray alloc] init] andManual:YES];
    
    //[Utility fireNotificationForDates:convertedDate andTitles:self.titleTxt.text andId:notificationStr andIsLast:NO];
    [[Utility managedObjectContext] save:nil];
    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initFadeAlertWithTitle:@"Success" andText:@"Congratulations! The Yartzeit has been added successfully." andCancelButton:NO forAlertType:AlertInfo andColor:[UIColor colorWithRed:(97.0/255.0) green:(75.0/255.0) blue:(234.0/255.0) alpha:1.0] withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
        [alert dismissAlertView];
        [self dismissViewControllerAnimated:YES completion:^{
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            if ([[appDelegate topViewController] isKindOfClass:[ViewController class]]) {
                ViewController *vc = (ViewController *)[appDelegate topViewController];
                [vc.calendarViewMain setSelectedDate:[NSDate date]];
            }
        }];
    }];
                                
    [alert.defaultButton setTitle:@"Close" forState:UIControlStateNormal];
    
    alert.cornerRadius = 10.0f;
    alert.logoView.image = [UIImage imageNamed:@"candle"];
    [alert show];
    
    
    
    
    
    
    
}

- (IBAction)cancelAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cameraAction:(UIButton *)sender {
    UIImagePickerController* imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = true;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:imagePicker animated:YES completion:nil];
    
    
}

- (IBAction)galleryAction:(UIButton *)sender {
    
    UIImagePickerController* imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = true;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:imagePicker animated:YES completion:nil];
    
    
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage* selectedImage = [self imageWithImage:info[UIImagePickerControllerEditedImage] scaledToWidth:self.nMainTV.frame.size.width-20.0];
    [picker dismissViewControllerAnimated:YES completion:^{
        [SwiftSpinner show:@"Uploading...0" animated:YES];
        [Utility uploadToAmazonS3WithData:[self compress:selectedImage] AndResponseBlock:^(id object, BOOL status, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SwiftSpinner hide:nil];
                if (error) {
                    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.localizedDescription andCancelButton:NO forAlertType:AlertFailure];
                    alert.animationType = DropAnimation;
                    alert.logoView.image = [UIImage imageNamed:@"candle"];
                    [alert show];
                }
                else{
                    NSMutableAttributedString *alreadyHtml = [self.nMainTV.attributedText mutableCopy];
                    
                    
                    
                    
                    
                    
                    NSAttributedString *imageStr = [[NSAttributedString alloc] initWithData:[[NSString stringWithFormat:@"<br>*<img src=\"%@\"/>", [object objectForKey:@"url"]] dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                          documentAttributes:nil error:nil];
                    
                    
                    [alreadyHtml appendAttributedString:imageStr];
                    
                    
                    [uploadedImagesUrls addObject:[NSString stringWithFormat:@"<br><center><img src=\"%@\"/></center>", [object objectForKey:@"url"]]];
                    
                    self.nMainTV.attributedText = alreadyHtml;
                    self.nMainTV.font = self.titleTxt.font;
                    self.nMainTV.textColor = [UIColor whiteColor];
                    NSLog(@"Image url is %@" , [object objectForKey:@"url"]);
                }
                
                
            });
            
        } AndProgressBlock:^(CGFloat progress) {
            int progressInt = (int)progress;
            NSString *uploadingStr = [NSString stringWithFormat:@"Uploading...%d ",progressInt];
            dispatch_async(dispatch_get_main_queue(), ^{
                [SwiftSpinner show:uploadingStr animated:YES];
            });
        }];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(NSData*)compress:(UIImage*)image {
    int kMaxUploadSize = 150000;
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.1f;
    NSData* imageData;
    do {
        imageData = UIImageJPEGRepresentation(image, compression);
        compression -= 0.10;
    } while ([imageData length] > kMaxUploadSize && compression > maxCompression);
    
    return imageData;
}




@end
