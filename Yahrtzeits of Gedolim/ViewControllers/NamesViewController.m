

//
//  NamesViewController.m
//  Yahrtzeits of Gedolim
//
//  Created by iBuildX on 27/06/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import "NamesViewController.h"
#import "CalendarSubjectsTableViewCell.h"
#import "Date+CoreDataClass.h"
#import "Date+CoreDataProperties.h"
#import <CoreData/CoreData.h>
#import "Utility.h"
#import <LinqToObjectiveC/LinqToObjectiveC.h>
#import "DetailsViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import <SwiftSpinner/SwiftSpinner-Swift.h>


@interface NamesViewController (){
    NSMutableArray *allNames;
    
    
    NSMutableArray *allNamesOriginal ;
    
    NSDictionary *allNamesDict;
    NSArray *allNamesDictTitles;
    NSArray *allAlphabets;
    
    
    Date *selectedDate;
}

@end

@implementation NamesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.allNamesTableView.delegate = self;
    self.allNamesTableView.dataSource = self;
    self.nameSearchbar.delegate = self;

    allAlphabets = @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z"];
    self.isAfterDelete = NO;

    
}

- (void)viewWillAppear:(BOOL)animated{
    self.nameSearchbar.text = @"";
    [SwiftSpinner show:@"Checking..." animated:YES];
    [Utility getCurrentDatesIdsAndResponseBlock:^(id object, BOOL status, NSError *error) {
        [SwiftSpinner hide:nil];
        [self getAllDates];
    }];

}

- (void)viewDidAppear:(BOOL)animated{
    [self.nameSearchbar resignFirstResponder];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getAllDates {
    NSError *error;
    
    NSFetchRequest *fetchRequest = [Date fetchRequest];
    
    allNames = [[[Utility managedObjectContext] executeFetchRequest:fetchRequest error:&error] mutableCopy];
    allNamesOriginal = [[[Utility managedObjectContext] executeFetchRequest:fetchRequest error:&error] mutableCopy];

    if (error) {
        NSLog(@"ERROR IN notificationId == %@" , error.description);
    }
    
    
    [self createDictionaryOfArrays];
}

-(void)createDictionaryOfArrays {
    NSLog(@"allNames Count = %lu" , (unsigned long)allNames.count) ;
    allNamesDict = [allNames linq_groupBy:^id(Date *date) {
        return [date.subjectTitle substringToIndex:1];
    }];
    allNamesDictTitles = [[allNamesDict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    NSLog(@"allNames Dictionary Titles Count= %lu" , (unsigned long)allNamesDictTitles.count) ;
    [self.allNamesTableView reloadData];
}


#pragma mark - Table View Delegate

#pragma mark - TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    // Return the number of sections.
    return [allNamesDictTitles count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [allNamesDictTitles objectAtIndex:section];
}


- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    return allAlphabets;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    return [allNamesDictTitles indexOfObject:title];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSString *sectionTitle = [allNamesDictTitles objectAtIndex:section];
    NSArray *sectionAnimals = [allNamesDict objectForKey:sectionTitle];
    return [sectionAnimals count];
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *sectionTitle = [allNamesDictTitles objectAtIndex:indexPath.section];
    NSArray *sectionAnimals = [allNamesDict objectForKey:sectionTitle];
    [tableView deselectRowAtIndexPath:indexPath animated:YES] ;
    [self.nameSearchbar resignFirstResponder];
    Date *date = [sectionAnimals objectAtIndex:indexPath.row];
    selectedDate = date;
    [self performSegueWithIdentifier:@"ToDetials" sender:date];
}





- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CalendarSubjectsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CalendarSubjectsTableViewCell"];
    
    NSString *sectionTitle = [allNamesDictTitles objectAtIndex:indexPath.section];
    NSArray *sectionAnimals = [allNamesDict objectForKey:sectionTitle];
    
    
    Date *date = [sectionAnimals objectAtIndex:indexPath.row];
    
    NSString * const kAPIDateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSSSSS";
    
    // convert API date string
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:kAPIDateFormat];
    cell.hbDayText.text = [NSString stringWithFormat:@"%i" , date.day];
    
    
    
    NSMutableArray *hebrewMonths = [[NSMutableArray alloc] initWithObjects:@"Nisan",
                    @"Iyyar" ,
                    @"Sivan",
                    @"Tamuz",
                    @"Av",
                    @"Elul",
                    @"Tishrei",
                    @"Cheshvan",
                    @"Kislev",
                    @"Tevet",
                    @"Sh'vat",
                    @"Adar",
                    @"Adar II", nil];
    
    cell.grDauText.text = hebrewMonths[date.month - 1];
    
    cell.grDauText.font = [UIFont fontWithName:cell.grDauText.font.fontName size:12];
    
    cell.subjectTitleText.text = date.subjectTitle;
    cell.subjectSubTitleText.text = date.subjectTitle;
    return cell;
}

#pragma mark - Search Bar Delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    NSLog(@"Text is %@" ,searchText ) ;
    if (searchText.length == 0) {
        allNames = [[NSMutableArray alloc] initWithArray:allNamesOriginal] ;
    }
    else{
        allNames = [[NSMutableArray alloc] init] ;
        for (Date *date in allNamesOriginal) {
            if ([[date.subjectTitle lowercaseString] containsString:[searchText lowercaseString]]) {
                [allNames addObject:date] ;
            }
        }
    }
    [self createDictionaryOfArrays] ;
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.destinationViewController isKindOfClass:[DetailsViewController class]]) {
        DetailsViewController *vc = segue.destinationViewController;
        vc.selectedCoreDataDate = selectedDate;
        
    }
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)closeAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        if (self.isAfterDelete) {
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            if ([[appDelegate topViewController] isKindOfClass:[ViewController class]]) {
                ViewController *vc = (ViewController *)[appDelegate topViewController];
                [vc.calendarViewMain setSelectedDate:[NSDate date]];
            }
        }
    }];
    
}

- (IBAction)addAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"ToAdd" sender:self];
}
@end
