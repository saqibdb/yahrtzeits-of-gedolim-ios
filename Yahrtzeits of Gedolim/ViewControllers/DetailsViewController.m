



//
//  DetailsViewController.m
//  Yahrtzeits of Gedolim
//
//  Created by iBuildX on 04/06/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import "DetailsViewController.h"
#import <YLGIFImage/YLGIFImage.h>
#import "AMSmoothAlertView.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "NamesViewController.h"
#import "SetAsLinkSupport.h"



@interface DetailsViewController ()

@end

@implementation DetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (self.selectedCoreDataDate) {
        if([self.selectedCoreDataDate.notificationId containsString:@"-Gedol-Manual"]){
            self.binBtn.hidden = NO;
        }
        else{
            self.binBtn.hidden = YES;
        }
        
        self.detailsTextView.attributedText = [[NSAttributedString alloc] initWithData:[self.selectedCoreDataDate.subjectDescription dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                                    documentAttributes:nil error:nil];
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"MMM"];
        self.grMonthText.text = [df stringFromDate:self.selectedCoreDataDate.grDate];
        [df setDateFormat:@"dd"];
        self.grDateText.text = [df stringFromDate:self.selectedCoreDataDate.grDate];
        
        NSMutableArray *hebrewMonths = [[NSMutableArray alloc] initWithObjects:@"Nisan",
                                        @"Iyyar" ,
                                        @"Sivan",
                                        @"Tamuz",
                                        @"Av",
                                        @"Elul",
                                        @"Tishrei",
                                        @"Cheshvan",
                                        @"Kislev",
                                        @"Tevet",
                                        @"Sh'vat",
                                        @"Adar",
                                        @"Adar II", nil];
        
        self.hbMonthText.text = hebrewMonths[self.selectedCoreDataDate.month - 1];        
        self.hbDateTxt.text = [NSString stringWithFormat:@"%i" , self.selectedCoreDataDate.day];
    }
    else{
        
        NSError *errorCoreData;
        
        NSFetchRequest *fetchRequest = [Date fetchRequest];
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"notificationId contains[cd] %@", @"-Gedol-Manual"];

        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"subjectTitle contains[cd] %@", self.selectedSubjectModel.subjectTitle];
       
        NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
        fetchRequest.predicate = predicate;
        NSArray *datesAddedManually = [[Utility managedObjectContext] executeFetchRequest:fetchRequest error:&errorCoreData] ;
        
        if (errorCoreData) {
            NSLog(@"ERROR IN notificationId == %@" , errorCoreData.description);
        }
        
        if (datesAddedManually.count) {
            NSLog(@"THERE IS CORE DATA MANUAL");
            self.binBtn.hidden = NO;
            self.selectedCoreDataDate = [datesAddedManually firstObject];
        }
        else{
            self.binBtn.hidden = YES;
        }
        
        
        
        
        self.detailsTextView.attributedText = [[NSAttributedString alloc] initWithData:[self.selectedSubjectModel.subjectDescription dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                                    documentAttributes:nil error:nil];
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"MMM"];
        self.grMonthText.text = [df stringFromDate:self.selectedDate];
        [df setDateFormat:@"dd"];
        self.grDateText.text = [df stringFromDate:self.selectedDate];
        
        
        
        self.hbMonthText.text = self.selectedHebrewDate.hm;
        self.hbDateTxt.text = [self.selectedHebrewDate.hd stringValue];
    }
    
    
    
    
    self.gifImageView.image = [YLGIFImage imageNamed:@"giphy.gif"];
    
    [self.detailsTextView scrollsToTop];
    [self.detailsTextView setContentOffset:CGPointZero animated:YES];


}
    
-(void)viewDidAppear:(BOOL)animated{
    [self.detailsTextView setContentOffset:CGPointZero animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)closeAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)shareAction:(UIButton *)sender {
    self.toolTipView.hidden = !self.toolTipView.hidden;
    
    return;
    
    
    
    
    
    
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Share" message:@"Share this with:" preferredStyle:UIAlertControllerStyleActionSheet];
    NSMutableString *bodyText = [NSMutableString string];
    NSString *subjectDescription = self.selectedSubjectModel.subjectDescription;
    NSRange range = [subjectDescription rangeOfString:@"</h1>\r\n"];
    subjectDescription = [subjectDescription substringFromIndex:range.location];
    
    
    [bodyText appendString:[self stringByStrippingHTML:subjectDescription]];
    [bodyText appendString:[NSString stringWithFormat:@"\n\nDate : %@", self.selectedHebrewDate.hebrew]];
    [bodyText appendString:@"\n\nInfo from the Gedolim Yartzeit App. Get it here:\n\niOS Download = https://itunes.apple.com/us/app/gedolim-yartzeits/id1439800530?ls=1&mt=8 \nAndroid Download = https://play.google.com/store/apps/details?id=com.saqibdb.YahrtzeitsOfGedolim"];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"SMS" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if(![MFMessageComposeViewController canSendText]) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Your device doesn't support SMS!" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];
            return;
        }
        MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
        messageController.messageComposeDelegate = self;
        [messageController setBody:bodyText];
        
        
        // Present message view controller on screen
        [self presentViewController:messageController animated:YES completion:nil];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if ([MFMailComposeViewController canSendMail]) {
            MFMailComposeViewController * mail = [[MFMailComposeViewController alloc]init];
            mail.mailComposeDelegate = self;
            [mail setMessageBody:bodyText isHTML:YES];
            [self presentViewController:mail animated:YES completion:NULL];
        }
        else if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[@"mailto://" stringByAppendingString:@""]]]){
            NSString* to = @"";
            NSString *encodedTo = [to stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSString *encodedURLString = [NSString stringWithFormat:@"mailto:%@?%@&%@", encodedTo, @"Share", bodyText];
            NSURL *mailtoURL = [NSURL URLWithString:encodedURLString];
            [[UIApplication sharedApplication] openURL:mailtoURL options:@{} completionHandler:nil];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Cannot send email" message:@"Email is not available. Please check your settings." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:true completion:nil];
        }
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
    
    
}

- (IBAction)smsAction:(UIButton *)sender {
    self.toolTipView.hidden = YES;
    NSMutableString *bodyText = [NSMutableString string];
    NSString *titleText = self.selectedSubjectModel.subjectTitle;
    NSString *subjectDescription = self.selectedSubjectModel.subjectDescription;
    
    if (self.selectedCoreDataDate) {
        subjectDescription = self.selectedCoreDataDate.subjectDescription;
        titleText = self.selectedCoreDataDate.subjectTitle;
    }
    
    if (self.selectedCoreDataDate) {
        NSDictionary *monthsDict = @{@1 : @"Nisan",
                                     @2 : @"Iyar" ,
                                     @3 : @"Sivan",
                                     @4 : @"Tamuz",
                                     @5 : @"Av",
                                     @6 : @"Elul",
                                     @7 : @"Tishri",
                                     @8 : @"Heshvan",
                                     @9 : @"Kislev",
                                     @10 : @"Tevet",
                                     @11 : @"Shevat",
                                     @12 : @"Adar I",
                                     @13 : @"Adar II"
                                     };
        
        NSString *hebrewMonthName = monthsDict[@(self.selectedCoreDataDate.month)] ;
        [bodyText appendString:[NSString stringWithFormat:@"\n\nYartzeit : %@", [NSString stringWithFormat:@"%i %@",self.selectedCoreDataDate.day, hebrewMonthName]]];
    }
    else{
        [bodyText appendString:[NSString stringWithFormat:@"Yartzeit : %@\n\n", self.selectedHebrewDate.hebrew]];
    }
    
    if ([subjectDescription containsString:@"</h1>\r\n"]) {
        NSRange range = [subjectDescription rangeOfString:@"</h1>\r\n"];
        subjectDescription = [subjectDescription substringFromIndex:range.location];
    }
    [bodyText appendString:[self stringByStrippingHTML:subjectDescription]];
    
    
    
    [bodyText appendString:@"\n\nInfo from the Gedolim Yartzeit App. Get it here:\n\n\niOS Download = https://itunes.apple.com/us/app/gedolim-yartzeits/id1439800530?ls=1&mt=8 \n\nAndroid Download = https://play.google.com/store/apps/details?id=com.saqibdb.YahrtzeitsOfGedolim"];
    
    

    
    
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Your device doesn't support SMS!" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setBody:bodyText];
    [messageController setTitle:titleText];
    
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
}

- (IBAction)emailAction:(UIButton *)sender {
    self.toolTipView.hidden = YES;
    NSMutableString *bodyText = [NSMutableString string];
    NSString *subjectDescription = self.selectedSubjectModel.subjectDescription;
    NSString *titleText = self.selectedSubjectModel.subjectTitle;
    if (self.selectedCoreDataDate) {
        subjectDescription = self.selectedCoreDataDate.subjectDescription;
        titleText = self.selectedCoreDataDate.subjectTitle;
    }
    if ([subjectDescription containsString:@"</h1>\r\n"]) {
        NSRange range = [subjectDescription rangeOfString:@"</h1>\r\n"];
        subjectDescription = [subjectDescription substringFromIndex:range.location];
    }
    
    
    [bodyText appendString:[self stringByStrippingHTML:subjectDescription]];
    if (self.selectedCoreDataDate) {
        NSDictionary *monthsDict = @{@1 : @"Nisan",
                                     @2 : @"Iyar" ,
                                     @3 : @"Sivan",
                                     @4 : @"Tamuz",
                                     @5 : @"Av",
                                     @6 : @"Elul",
                                     @7 : @"Tishri",
                                     @8 : @"Heshvan",
                                     @9 : @"Kislev",
                                     @10 : @"Tevet",
                                     @11 : @"Shevat",
                                     @12 : @"Adar I",
                                     @13 : @"Adar II"
                                     };
        
        NSString *hebrewMonthName = monthsDict[@(self.selectedCoreDataDate.month)] ;
        [bodyText appendString:[NSString stringWithFormat:@"\n\nYartzeit : %@", [NSString stringWithFormat:@"%i %@",self.selectedCoreDataDate.day, hebrewMonthName]]];
    }
    else{
        [bodyText appendString:[NSString stringWithFormat:@"\n\nYartzeit : %@", self.selectedHebrewDate.hebrew]];
    }
    [bodyText appendString:@"\n\nInfo from the Gedolim Yartzeit App. Get it here:\n\niOS Download = https://itunes.apple.com/us/app/gedolim-yartzeits/id1439800530?ls=1&mt=8 \nAndroid Download = https://play.google.com/store/apps/details?id=com.saqibdb.YahrtzeitsOfGedolim"];
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController * mail = [[MFMailComposeViewController alloc]init];
        mail.mailComposeDelegate = self;
        [mail setMessageBody:bodyText isHTML:YES];
        [mail setSubject:titleText];
        [self presentViewController:mail animated:YES completion:NULL];
    }
    else if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[@"mailto://" stringByAppendingString:@""]]]){
        NSString* to = @"";
        NSString *encodedTo = [to stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodedURLString = [NSString stringWithFormat:@"mailto:%@?%@&%@", encodedTo, @"Share", bodyText];
        NSURL *mailtoURL = [NSURL URLWithString:encodedURLString];
        [[UIApplication sharedApplication] openURL:mailtoURL options:@{} completionHandler:nil];
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Cannot send email" message:@"Email is not available. Please check your settings." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:true completion:nil];
    }
}

- (IBAction)binAction:(UIButton *)sender {
    if(self.selectedCoreDataDate == nil){
        NSError *errorCoreData;
        
        NSFetchRequest *fetchRequest = [Date fetchRequest];
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"notificationId contains[cd] %@", @"-Gedol-Manual"];
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"subjectTitle contains[cd] %@",self.selectedSubjectModel.subjectTitle];
        NSPredicate *predicate3 =  [NSPredicate predicateWithFormat:@"subjectDescription contains[cd] %@",self.selectedSubjectModel.subjectDescription];
        NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2 , predicate3]];
        fetchRequest.predicate = predicate;
        NSArray *datesAddedManually = [[Utility managedObjectContext] executeFetchRequest:fetchRequest error:&errorCoreData] ;
        
        if (errorCoreData) {
            NSLog(@"ERROR IN notificationId == %@" , errorCoreData.description);
        }
        
        if (datesAddedManually.count) {
            self.selectedCoreDataDate = [datesAddedManually firstObject];
        }
        else{
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initFadeAlertWithTitle:@"Stop" andText:@"You Cannot delete Yartzeits from server." andCancelButton:NO forAlertType:AlertInfo andColor:[UIColor colorWithRed:(255.0/255.0) green:(75.0/255.0) blue:(13.0/255.0) alpha:1.0] withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
                [alert dismissAlertView];
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alert.defaultButton setTitle:@"Close" forState:UIControlStateNormal];
            
            alert.cornerRadius = 10.0f;
            alert.logoView.image = [UIImage imageNamed:@"candle"];
            [alert show];
            return;
        }

        
    }
    [[Utility managedObjectContext] deleteObject:self.selectedCoreDataDate];
    __block NSError *errorCoreData;
    dispatch_async(dispatch_get_main_queue(), ^{
        [[Utility managedObjectContext] save:&errorCoreData];
    });
    
    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initFadeAlertWithTitle:@"Deleted" andText:@"Yartzeit has been deleted." andCancelButton:NO forAlertType:AlertInfo andColor:[UIColor colorWithRed:(255.0/255.0) green:(75.0/255.0) blue:(13.0/255.0) alpha:1.0] withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
        [alert dismissAlertView];
        [self dismissViewControllerAnimated:YES completion:^{
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            if ([[appDelegate getVisibleViewController] isKindOfClass:[ViewController class]]) {
                ViewController *vc = (ViewController *)[appDelegate getVisibleViewController];
                vc.isAfterDelete = YES;
                [vc.calendarViewMain setSelectedDate:[NSDate date]];
            }
            else if ([[appDelegate getVisibleViewController] isKindOfClass:[NamesViewController class]]) {
                NamesViewController *vc = (NamesViewController *)[appDelegate getVisibleViewController];

            }
        }];
    }];
    
    [alert.defaultButton setTitle:@"Close" forState:UIControlStateNormal];
    
    alert.cornerRadius = 10.0f;
    alert.logoView.image = [UIImage imageNamed:@"candle"];
    [alert show];
}




#pragma mark - Mail Delegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    [controller dismissViewControllerAnimated:YES completion:nil];
    if (result == MessageComposeResultCancelled)
        NSLog(@"Message cancelled");
    else if (result == MessageComposeResultSent){
        NSLog(@"Message sent");
    }
    else
        NSLog(@"Message failed");
}



-(NSString *)stringByStrippingHTML:(NSString*)str{
    NSRange r;
    while ((r = [str rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location     != NSNotFound)
    {
        str = [str stringByReplacingCharactersInRange:r withString:@""];
    }
    return str;
}
@end
