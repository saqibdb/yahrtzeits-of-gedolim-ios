//
//  AddSubjectViewController.h
//  Yahrtzeits of Gedolim
//
//  Created by iBuildX on 19/08/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ACFloatingTextfield_Objc/ACFloatingTextField.h>
#import "Yahrtzeits_of_Gedolim-Swift.h"




@interface AddSubjectViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView *hebrewDayPicker;
@property (weak, nonatomic) IBOutlet UIPickerView *hebrewMonthPicker;


@property (weak, nonatomic) IBOutlet ACFloatingTextField *titleTxt;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *toolbarBottomConstraint;


@property (weak, nonatomic) IBOutlet UIButton *convertBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *addBtn;

@property (weak, nonatomic) IBOutlet UITextView *nMainTV;










- (IBAction)closeAction:(UIButton *)sender;
- (IBAction)convertAction:(UIButton *)sender;

- (IBAction)addAction:(UIButton *)sender;
- (IBAction)cancelAction:(UIButton *)sender;

- (IBAction)cameraAction:(UIButton *)sender;
- (IBAction)galleryAction:(UIButton *)sender;





@end
