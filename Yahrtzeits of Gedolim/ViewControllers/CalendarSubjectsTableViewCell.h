//
//  CalendarSubjectsTableViewCell.h
//  Yahrtzeits of Gedolim
//
//  Created by iBuildX on 30/05/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalendarSubjectsTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *hbDayText;
@property (weak, nonatomic) IBOutlet UILabel *grDauText;


@property (weak, nonatomic) IBOutlet UILabel *subjectTitleText;

@property (weak, nonatomic) IBOutlet UILabel *subjectSubTitleText;





@end
