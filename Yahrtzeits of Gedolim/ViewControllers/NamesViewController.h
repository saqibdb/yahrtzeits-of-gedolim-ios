//
//  NamesViewController.h
//  Yahrtzeits of Gedolim
//
//  Created by iBuildX on 27/06/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import "ViewController.h"

@interface NamesViewController : ViewController<UITableViewDelegate, UITableViewDataSource ,UISearchBarDelegate>


@property (weak, nonatomic) IBOutlet UISearchBar *nameSearchbar;
@property (weak, nonatomic) IBOutlet UITableView *allNamesTableView;







- (IBAction)closeAction:(UIButton *)sender;
- (IBAction)addAction:(UIButton *)sender;


@end
