//
//  DetailsViewController.h
//  Yahrtzeits of Gedolim
//
//  Created by iBuildX on 04/06/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Yahrtzeits_of_Gedolim-Swift.h"
#import "Utility.h"
#import "SubjectModel.h"
#import "DateModel.h"
#import <YLGIFImage/YLImageView.h>
#import "Date+CoreDataClass.h"
#import "Date+CoreDataProperties.h"
#import <CoreData/CoreData.h>
#import <MessageUI/MessageUI.h>



@interface DetailsViewController : UIViewController<MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *grMonthText;
@property (weak, nonatomic) IBOutlet UILabel *grDateText;
@property (weak, nonatomic) IBOutlet UILabel *hbMonthText;
@property (weak, nonatomic) IBOutlet UILabel *hbDateTxt;



@property (weak, nonatomic) IBOutlet UITextView *detailsTextView;

@property (weak, nonatomic) IBOutlet YLImageView *gifImageView;

@property (weak, nonatomic) IBOutlet UIView *toolTipView;

@property (weak, nonatomic) IBOutlet UIButton *binBtn;








@property (copy, nonatomic) NSDate *selectedDate;
@property (copy, nonatomic) DateModel *selectedHebrewDate;
@property (copy, nonatomic) SubjectModel *selectedSubjectModel;


@property (retain, nonatomic) Date *selectedCoreDataDate;



- (IBAction)closeAction:(UIButton *)sender;
- (IBAction)shareAction:(UIButton *)sender;
- (IBAction)smsAction:(UIButton *)sender;
- (IBAction)emailAction:(UIButton *)sender;
- (IBAction)binAction:(UIButton *)sender;







@end
