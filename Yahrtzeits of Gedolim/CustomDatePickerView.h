//
//  CustomDatePickerView.h
//  Yahrtzeits of Gedolim
//
//  Created by iBuildX on 06/09/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomDatePickerView : UIView


@property (weak, nonatomic) IBOutlet UIDatePicker *mainDatePicker;


@property (weak, nonatomic) IBOutlet UISwitch *afterSunsetSwitch;



@end
