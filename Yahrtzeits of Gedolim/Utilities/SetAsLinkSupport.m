//
//  SetAsLinkSupport.m
//  Yahrtzeits of Gedolim
//
//  Created by ibuildx Macbook 1 on 3/13/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

#import "SetAsLinkSupport.h"
#import <UIKit/UIKit.h>
@implementation NSMutableAttributedString (SetAsLinkSupport)
- (BOOL)setAsLink:(NSString*)textToFind linkURL:(NSString*)linkURL {
    
    NSRange foundRange = [self.mutableString rangeOfString:textToFind];
    if (foundRange.location != NSNotFound) {
        [self addAttribute:NSLinkAttributeName value:linkURL range:foundRange];
        return YES;
    }
    return NO;
}
@end
