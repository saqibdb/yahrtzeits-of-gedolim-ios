//
//  Utility.m
//  OpenWorkr
//
//  Created by iBuildx_Mac_Mini on 12/15/16.
//  Copyright © 2016 Eden. All rights reserved.
//

#import "Utility.h"
#import "SQBConstants.h"
#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "SubjectModel.h"
#import "DateModel.h"
#import <LinqToObjectiveC/LinqToObjectiveC.h>
#import <AWSS3/AWSS3.h>
#import <EventKit/EventKit.h>
#import "Yahrtzeits_of_Gedolim-Swift.h"

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@implementation Utility



+(void)getHebrewDateFromLink:(NSString *)linkString AndResponseBlock:(APIRequestResponseBlock)responseBlock {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:linkString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        responseBlock(responseObject , YES , nil);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        responseBlock(nil , NO , error);
    }];
    
    
}

+(void)getSubjectsForDateWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock {
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",kBASE_URL , kGetInformationOnDate] ;

    [manager POST:requestUrl parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        responseBlock(responseObject , YES , nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        responseBlock(nil , NO , error);
    }];
}

+(void)getSubjectsBeforeDateWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock {
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",kBASE_URL , kGetInformationBeforeDate] ;
    
    [manager POST:requestUrl parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *errorDecode;
        SubjectDateModel *allDateSubjects = [[SubjectDateModel alloc] initWithDictionary:responseObject error:&errorDecode];
        if (errorDecode) {
            NSLog(@"ERROR ON PYTHON DECODE = %@", errorDecode.description);
            responseBlock(nil , NO , errorDecode);
        }
        
        NSMutableArray *newFoundSubjects = [[NSMutableArray alloc] init];
        
        
        for (SubjectModel *subject in allDateSubjects.Dates) {
            
            
            NSFetchRequest *fetchRequest = [Date fetchRequest];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %i",[subject.id intValue]];
            fetchRequest.predicate = predicate;
            
            NSArray *foundDates = [[Utility managedObjectContext] executeFetchRequest:fetchRequest error:nil] ;
            
            if (foundDates.count) {
                Date *newDate = [foundDates firstObject];
                newDate.id = [subject.id intValue];
                newDate.day = [subject.day intValue];
                newDate.month = [subject.month intValue];
                newDate.subjectTitle = subject.subjectTitle;
                newDate.subjectDescription = subject.subjectDescription;
                //"2018-06-07T17:14:32.268580Z"
                NSString * const kAPIDateFormat = @"yyyy-MM-dd HH:mm:ss";
                
                // convert API date string
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:kAPIDateFormat];
                newDate.created_date =  [formatter dateFromString:[subject.created_date stringByReplacingOccurrencesOfString:@"Z" withString:@""]];
                newDate.modified_date =  [formatter dateFromString:[subject.modified_date stringByReplacingOccurrencesOfString:@"Z" withString:@""]];
                newDate.notificationId = nil;
                
                [newFoundSubjects addObject:newDate];
            }
            else{
                Date *newDate = [[Date alloc] initWithContext:[Utility managedObjectContext]];
                newDate.id = [subject.id intValue];
                newDate.day = [subject.day intValue];
                newDate.month = [subject.month intValue];
                newDate.subjectTitle = subject.subjectTitle;
                newDate.subjectDescription = subject.subjectDescription;
                //"2018-06-07T17:14:32.268580Z"
                NSString * const kAPIDateFormat = @"yyyy-MM-dd HH:mm:ss";

                // convert API date string
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:kAPIDateFormat];
                newDate.created_date =  [formatter dateFromString:[subject.created_date stringByReplacingOccurrencesOfString:@"Z" withString:@""]];
                newDate.modified_date =  [formatter dateFromString:[subject.modified_date stringByReplacingOccurrencesOfString:@"Z" withString:@""]];
                newDate.notificationId = nil;
                
                [newFoundSubjects addObject:newDate];
            }
        }
        
        __block NSError *error;
        dispatch_async(dispatch_get_main_queue(), ^{
            [[Utility managedObjectContext] save:&error];
        });
        
        if(error){
            NSLog(@"ERROR IN COREDATA = %@" , error.description);
            responseBlock(nil , NO , error);
        }
        else{
            
            
            NSFetchRequest *fetchRequest = [Date fetchRequest];
            NSArray *allDates = [[Utility managedObjectContext] executeFetchRequest:fetchRequest error:nil] ;
            
            for (Date *date in allDates) {
                NSLog(@"Date = %@" , date.grDate);
            }
            
            
            responseBlock(newFoundSubjects , YES , nil);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        responseBlock(nil , NO , error);
    }];
}






+(void)checkForNewDates {
    
    [self integrateNotificationsForOldDates];
    
    
    NSFetchRequest *fetchRequest = [Date fetchRequest];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"modified_date = self.@max.modified_date"];
    fetchRequest.predicate = predicate;
    
    NSArray *maxDates = [[Utility managedObjectContext] executeFetchRequest:fetchRequest error:nil] ;
    NSString *lastModifiedDate = @"1970-06-07T16:50:06.544459Z";
    
    if (maxDates.count == 0) {
        
    }
    else{
        Date *maxDateObj = [maxDates firstObject];
        NSLog(@"MAX DATE IS = %@" , maxDateObj.modified_date);
        
        NSString * const kAPIDateFormat = @"yyyy-MM-dd HH:mm:ss";

        // convert API date string
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:kAPIDateFormat];
        lastModifiedDate = [NSString stringWithFormat:@"%@Z",[formatter stringFromDate:[maxDateObj.modified_date dateByAddingTimeInterval:1.0]]];
        
        //lastModifiedDate = [NSString stringWithFormat:@"%@Z",[formatter stringFromDate:maxDateObj.modified_date]];

    }
    
    NSDictionary *dict = @{ @"modified_date" : lastModifiedDate, @"page" : @"-1"};

    [self getSubjectsBeforeDateWithDictionary:dict AndResponseBlock:^(id object, BOOL status, NSError *error) {
        if (status) {
            NSMutableArray *allNewObjects = (NSMutableArray *)object;
            NSLog(@"NEW OBJECTS COUNT = %li" , allNewObjects.count);
            
        }
        else{
            NSLog(@"ERROR ON BEFORE DATE = %@",error.description);
        }
        
        
        NSFetchRequest *fetchRequest = [Date fetchRequest];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"grDate == NULL"];
        fetchRequest.predicate = predicate;
        
        NSArray *datesWithoutGr = [[Utility managedObjectContext] executeFetchRequest:fetchRequest error:nil] ;
        NSLog(@"NEW OBJECTS COUNT = %li" , datesWithoutGr.count);
        if (datesWithoutGr.count) {
            NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            NSCalendar * hebrew = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierHebrew];
            
            
            
            NSDateComponents * components = [gregorian components:NSUIntegerMax fromDate:[NSDate date]];
            NSDate * hebrewDateToday = [hebrew dateFromComponents:components];
            NSInteger hebrewYear = [hebrew component:NSCalendarUnitYear fromDate:hebrewDateToday];
            
            
            NSDateFormatter * hebrewYearFormatter = [[NSDateFormatter alloc] init];
            [hebrewYearFormatter setDateStyle:NSDateFormatterLongStyle];
            [hebrewYearFormatter setTimeStyle:NSDateFormatterNoStyle];
            [hebrewYearFormatter setCalendar:hebrew];
            [hebrewYearFormatter setDateFormat:@"yyyy-MM-dd"];
            //hebrewYear = [[formatter stringFromDate:[NSDate date]] integerValue];
            
            NSLog(@"Hebrew Today Formatter: %@", [hebrewYearFormatter stringFromDate:[NSDate date]]);
            NSLog(@"Hebrew Today Year: %li", (long)hebrewYear);

            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd"];
            
            
            NSDate *hebrewTodayDateNew = [hebrewYearFormatter dateFromString:[hebrewYearFormatter stringFromDate:[NSDate date]]];
            
            NSLog(@"Hebrew Today New: %@", hebrewTodayDateNew);

            
            
            
            
            
            
            
            
            
            for (Date *dateWithoutGr in datesWithoutGr) {
                NSDateComponents *hebrewComponents = [[NSDateComponents alloc] init];
                hebrewComponents.day = dateWithoutGr.day;
                hebrewComponents.month = dateWithoutGr.month;
                hebrewComponents.year = hebrewYear;
                NSDate * hebrewDate = [hebrew dateFromComponents:hebrewComponents];
                if ([hebrewDate compare:hebrewDateToday] != NSOrderedDescending) {
                    NSDateComponents *hebrewComponentsNextYear = [[NSDateComponents alloc] init];
                    hebrewComponentsNextYear.day = dateWithoutGr.day;
                    hebrewComponentsNextYear.month = dateWithoutGr.month;
                    hebrewComponentsNextYear.year = hebrewYear+1;
                    hebrewDate = [hebrew dateFromComponents:hebrewComponentsNextYear];
                }
                NSDateComponents * components = [hebrew components:NSUIntegerMax fromDate:hebrewDate];
                NSDate * grDate = [gregorian dateFromComponents:components];
                dateWithoutGr.grDate = grDate;
                
                
            }
            [[Utility managedObjectContext] save:&error];
            [self integrateNotificationsForDates];

            
            
            /*
            
            
            
            
            
            
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSInteger year = [calendar component:NSCalendarUnitYear fromDate:[NSDate date]];
            NSInteger month = [calendar component:NSCalendarUnitMonth fromDate:[NSDate date]];
            NSInteger day = [calendar component:NSCalendarUnitDay fromDate:[NSDate date]];
            
            NSString *linkString = [NSString stringWithFormat:@"http://www.hebcal.com/converter/?cfg=json&gy=%li&gm=%li&gd=%li&g2h=1", (long)year, (long)month, (long)day];
            [Utility getHebrewDateFromLink:linkString AndResponseBlock:^(id object, BOOL status, NSError *error) {
                if (status) {
                    NSError *error;
                    DateModel *date = [[DateModel alloc] initWithDictionary:object error:&error];
                    if (error) {
                        NSLog(@"ERROR ON JSONMODEL = %@" , error.description);
                    }
                    else{
                        
                        

                        [self callAsyncDateConvertorWithIndex:0 andArray:datesWithoutGr andCurrentHebrewDate:date];

                        
                        
                    }
                }
                else{
                    NSLog(@"ERROR ON RESPONSE = %@" , error.description);
                }
            }];
             */
        }
        else{
            [self integrateNotificationsForDates];
        }
    }];
}

+(void)callAsyncDateConvertorWithIndex :(int)index andArray :(NSArray *)dates andCurrentHebrewDate:(DateModel *)dateModelHebrew {
    NSDictionary *monthsDict = @{@1 : @"Nisan",
                                 @2 : @"Iyyar" ,
                                 @3 : @"Sivan",
                                 @4 : @"Tamuz",
                                 @5 : @"Av",
                                 @6 : @"Elul",
                                 @7 : @"Tishrei",
                                 @8 : @"Cheshvan",
                                 @9 : @"Kislev",
                                 @10 : @"Tevet",
                                 @11 : @"Sh'vat",
                                 @12 : @"Adar",
                                 @13 : @"Adar%20II"
                                 };
    Date *date = dates[index];
    NSString *year = [dateModelHebrew.hy stringValue];
    NSString *month = monthsDict[[NSNumber numberWithInteger:date.month]] ;
    NSString *day = [NSString stringWithFormat:@"%i",date.day];
    
    NSCalendar * hebrew = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierHebrew];
    NSDateComponents *hebrewComponents = [[NSDateComponents alloc] init];
    hebrewComponents.day = [day integerValue];
    hebrewComponents.month = date.month;
    hebrewComponents.year = [year integerValue];
    NSDate * hebrewDate = [hebrew dateFromComponents:hebrewComponents];
    if (hebrewDate) {
        if ([day isEqualToString:@"22"] && date.month == 1 && [year isEqualToString:@"5778"]) {
            NSLog(@"ISSUE HERE");
        }
        NSLog(@"H__%@-%i-%@ = %@",day, date.month, year , hebrewDate);
    }
    

    if ((index+1) < dates.count) {
        [self callAsyncDateConvertorWithIndex:(index+1) andArray:dates andCurrentHebrewDate:dateModelHebrew];
        NSLog(@"Async Called With Index = %i" , index+1);
    }
    else{
        NSLog(@"All Done");
        //[self integrateNotificationsForDates];
    }
    
    return;
    
    
    
    
    
    NSString *linkString = [NSString stringWithFormat:@"http://www.hebcal.com/converter/?cfg=json&hy=%@&hm=%@&hd=%@&h2g=1", year, month, day];
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    });
    [Utility getHebrewDateFromLink:linkString AndResponseBlock:^(id object, BOOL status, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        });
        if (status) {
            NSError *error;
            DateModel *dateGr = [[DateModel alloc] initWithDictionary:object error:&error];
            if (error) {
                NSLog(@"ERROR ON JSONMODEL = %@" , error.description);
                if ((index+1) < dates.count) {
                    [self callAsyncDateConvertorWithIndex:(index+1) andArray:dates andCurrentHebrewDate:dateModelHebrew];
                    NSLog(@"Async Called With Index = %i" , index+1);
                }
                else{
                    NSLog(@"All Done");
                    [self integrateNotificationsForDates];
                }
            }
            else{
                NSDateComponents *comps = [[NSDateComponents alloc] init];
                comps.day = [dateGr.gd integerValue];
                comps.month = [dateGr.gm integerValue];
                comps.year = [dateGr.gy integerValue];
                NSCalendar *calendar = [NSCalendar currentCalendar];
                NSDate *subjectDate = [calendar dateFromComponents:comps];
                date.grDate = subjectDate;
                date.year = [dateModelHebrew.hy integerValue];
                NSError *error;
                [[Utility managedObjectContext] save:&error];
                
                if(error){
                    NSLog(@"ERROR IN COREDATA = %@" , error.description);
                }
                else{
                    if ((index+1) < dates.count) {
                        [self callAsyncDateConvertorWithIndex:(index+1) andArray:dates andCurrentHebrewDate:dateModelHebrew];
                        NSLog(@"Async Called With Index = %i" , index+1);
                    }
                    else{
                        NSLog(@"All Done");
                        [self integrateNotificationsForDates];
                    }
                }
                
            }
        }
        else{
            NSLog(@"ERROR ON RESPONSE = %@" , error.description);
            if ((index+1) < dates.count) {
                [self callAsyncDateConvertorWithIndex:(index+1) andArray:dates andCurrentHebrewDate:dateModelHebrew];
                NSLog(@"Async Called With Index = %i" , index+1);
            }
            else{
                NSLog(@"All Done");
                [self integrateNotificationsForDates];
            }
        }
    }];
}


+(void)integrateNotificationsForDates{
    NSError *error;
    
    NSFetchRequest *fetchRequest = [Date fetchRequest];
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"notificationId == NULL"];
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"notificationId contains[cd] %@", @"no"];
    NSPredicate *predicate3 = [NSPredicate predicateWithFormat:@"notificationId contains[cd] %@", @"None"];
    NSPredicate *predicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate1, predicate2 , predicate3]];
    fetchRequest.predicate = predicate;
    NSArray *datesWithoutNotifications = [[Utility managedObjectContext] executeFetchRequest:fetchRequest error:&error] ;
    
    if (error) {
        NSLog(@"ERROR IN notificationId == %@" , error.description);
    }
    
    NSMutableArray *passedDates = [[NSMutableArray alloc] init];

    for (Date* date in datesWithoutNotifications) {
        
        NSDate *newDate = [[NSCalendar currentCalendar] dateBySettingHour:8 minute:0 second:0 ofDate:date.grDate options:0];

        if ([newDate compare:[NSDate date]] == NSOrderedDescending) {
            [self fireNotificationOnDate:date];
            NSLog(@"date will Fire = %@" , newDate);

        } else {
            NSLog(@"dates is passed");
            [passedDates addObject:date];
        }
    }
    if (passedDates.count) {
        //[self getNewDateForDateWithIndex:0 andArray:passedDates];
    }
}

+(void)integrateNotificationsForOldDates{
    NSFetchRequest *fetchRequest = [Date fetchRequest];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"grDate <= %@",[NSDate date]];
    fetchRequest.predicate = predicate;
    
    NSArray *datesWithNotificationsInPast = [[Utility managedObjectContext] executeFetchRequest:fetchRequest error:nil] ;
    
    NSLog(@"datesWithNotificationsInPast = %lu", (unsigned long)datesWithNotificationsInPast.count);
    NSMutableArray *passedDates = [[NSMutableArray alloc] init];
    for (Date* date in datesWithNotificationsInPast) {
        if ([date.grDate compare:[NSDate date]] == NSOrderedDescending) {
            [self fireNotificationOnDate:date];
        } else {
            NSLog(@"dates is passed");
            [passedDates addObject:date];
        }
    }
    if (passedDates.count) {
        [self getNewDateForDateWithIndex:0 andArray:passedDates];
    }
}


+(void)disableNotificationsForAllDates{
    NSFetchRequest *fetchRequest = [Date fetchRequest];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"notificationId != NULL"];
    fetchRequest.predicate = predicate;
    NSArray *datesWithNotifications = [[Utility managedObjectContext] executeFetchRequest:fetchRequest error:nil] ;
    NSMutableArray<NSString *> *notificationIds = [[NSMutableArray<NSString *> alloc] init];
    for (Date* date in datesWithNotifications) {
        [notificationIds addObject:date.notificationId];
        date.notificationId = @"no";
        
    }
    if (notificationIds.count) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center removePendingNotificationRequestsWithIdentifiers:notificationIds];
        [center removeDeliveredNotificationsWithIdentifiers:notificationIds];
        NSError *error1;
        [[Utility managedObjectContext] save:&error1];
        
        if(error1){
            NSLog(@"ERROR IN COREDATA1 = %@" , error1.description);
        }
    }
}

+(void)enableNotificationsForAllDates{
    NSFetchRequest *fetchRequest = [Date fetchRequest];
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"notificationId == NULL"];
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"notificationId contains[cd] %@", @"no"];
    NSPredicate *predicate3 = [NSPredicate predicateWithFormat:@"notificationId contains[cd] %@", @"None"];
    NSPredicate *predicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate1, predicate2 , predicate3]];
    fetchRequest.predicate = predicate;
    NSArray *datesWithNotifications = [[Utility managedObjectContext] executeFetchRequest:fetchRequest error:nil] ;
    NSMutableArray *passedDates = [[NSMutableArray alloc] init];
    for (Date* date in datesWithNotifications) {
        if ([date.grDate compare:[NSDate date]] == NSOrderedDescending) {
            [self fireNotificationOnDate:date];
        } else {
            NSLog(@"dates is passed");
            [passedDates addObject:date];
        }
    }
    if (passedDates.count) {
        [self getNewDateForDateWithIndex:0 andArray:passedDates];
    }
}

+(void)fireNotificationOnDate :(Date *)date {
    return;
    
    UNMutableNotificationContent *objNotificationContent = [[UNMutableNotificationContent alloc] init];
    
    objNotificationContent.title = [NSString localizedUserNotificationStringForKey:@"Today's Yartzeits!" arguments:nil];
    
    objNotificationContent.body = [NSString localizedUserNotificationStringForKey:date.subjectTitle arguments:nil];
    
    objNotificationContent.sound = [UNNotificationSound defaultSound];
    
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:date.grDate];

    /*
    NSDate *newDate = [[NSDate date] dateByAddingTimeInterval:5.0];

    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute |
                                    NSCalendarUnitSecond fromDate:newDate];
    */
    /*
    if (components.month == 6 && components.day == 15) {
        components.day = 14;
        
    }
    */
    components.hour = 8;
    components.minute = 0;
    components.second = 0;
    

//        components.hour = 15;
//        components.minute = 55;
//        components.second = 0;

    
    UNCalendarNotificationTrigger *trigger =  [UNCalendarNotificationTrigger triggerWithDateMatchingComponents:components repeats:NO];

    //UNTimeIntervalNotificationTrigger *trigger =  [UNTimeIntervalNotificationTrigger                                             triggerWithTimeInterval:3.f repeats:NO];
    NSString *notificationStr = [NSString stringWithFormat:@"%i-Gedol",date.id];

    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:notificationStr                                                                            content:objNotificationContent trigger:trigger];
    
    // 3. schedule localNotification
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    
    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        if (!error) {
            NSLog(@"Local Notification succeeded");
            date.notificationId = notificationStr;
            dispatch_async(dispatch_get_main_queue(), ^{
                NSError *error1;
                [[Utility managedObjectContext] save:&error1];
                
                if(error1){
                    NSLog(@"ERROR IN COREDATA1 = %@" , error1.description);
                }
            });
            
            
        } else {
            NSLog(@"Local Notification failed");
        }
    }];
}


+(void)getNewDateForDateWithIndex:(int)index andArray:(NSMutableArray *)allPassedDates {
    Date *date ;
    if (index >= allPassedDates.count) {
        return;
    }
    else{
        date = allPassedDates[index];
    }
    NSDictionary *monthsDict = @{@1 : @"Nisan",
                                 @2 : @"Iyyar" ,
                                 @3 : @"Sivan",
                                 @4 : @"Tamuz",
                                 @5 : @"Av",
                                 @6 : @"Elul",
                                 @7 : @"Tishrei",
                                 @8 : @"Cheshvan",
                                 @9 : @"Kislev",
                                 @10 : @"Tevet",
                                 @11 : @"Sh'vat",
                                 @12 : @"Adar",
                                 @13 : @"Adar%20II"
                                 };
    
    NSString *year = [NSString stringWithFormat:@"%i", (date.year+1)];
    NSString *month = monthsDict[[NSNumber numberWithInteger:date.month]] ;
    NSString *day = [NSString stringWithFormat:@"%i",date.day];
    
    NSString *linkString = [NSString stringWithFormat:@"http://www.hebcal.com/converter/?cfg=json&hy=%@&hm=%@&hd=%@&h2g=1", year, month, day];
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    });
    [Utility getHebrewDateFromLink:linkString AndResponseBlock:^(id object, BOOL status, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        });
        if (status) {
            NSError *error;
            DateModel *dateGr = [[DateModel alloc] initWithDictionary:object error:&error];
            if (error) {
                NSLog(@"ERROR ON JSONMODEL = %@" , error.description);
            }
            else{
                NSDateComponents *comps = [[NSDateComponents alloc] init];
                comps.day = [dateGr.gd integerValue];
                comps.month = [dateGr.gm integerValue];
                comps.year = [dateGr.gy integerValue];
                NSCalendar *calendar = [NSCalendar currentCalendar];
                NSDate *subjectDate = [calendar dateFromComponents:comps];
                date.grDate = subjectDate;
                date.year = [dateGr.hy integerValue];
                NSError *error;
                [[Utility managedObjectContext] save:&error];
                
                if(error){
                    NSLog(@"ERROR IN COREDATA = %@" , error.description);
                }
                else{
                    [self fireNotificationOnDate:date];
                }
                
            }
        }
        else{
            NSLog(@"ERROR ON RESPONSE = %@" , error.description);
        }
        [self getNewDateForDateWithIndex:index+1 andArray:allPassedDates];
    }];
}










+ (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

+(void)loginWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"application/json"];
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",kBASE_URL , kLogin] ;
    [manager POST:requestUrl parameters:dict progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"Data uploaded = %@" , uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"JSON: %@", responseObject);
        responseBlock(responseObject , YES , nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        responseBlock(nil , NO , error);
    }];
}

+(void)loginPostManWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock {
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",kBASE_URL , kLogin] ;
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"27ade19e-28c9-62d5-84e7-2559376ce068" };
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"%@", error);
            responseBlock(nil , NO , error);
            
        } else {
            NSError *errorJson=nil;
            NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorJson];
            
            responseBlock(responseDict , YES , nil);
        }
    }];
    [dataTask resume];
}




+(void)getUserForMapWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock {
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",kBASE_URL , kRadius] ;
    
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"27ade19e-28c9-62d5-84e7-2559376ce068" };
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"%@", error);
            responseBlock(nil , NO , error);
            
        } else {
            NSError *errorJson=nil;
            NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorJson];
            
            responseBlock(responseDict , YES , nil);
        }
    }];
    [dataTask resume];
}

+(void)updateUserWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock {
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",kBASE_URL , kUpdate] ;
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"27ade19e-28c9-62d5-84e7-2559376ce068" };
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"%@", error);
            responseBlock(nil , NO , error);
            
        } else {
            NSError *errorJson=nil;
            NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorJson];
            
            responseBlock(responseDict , YES , nil);
        }
    }];
    [dataTask resume];
}


+(void)uploadProfilePictureWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock {
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",kBASE_URL , kUpload] ;
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"27ade19e-28c9-62d5-84e7-2559376ce068" };
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"%@", error);
            responseBlock(nil , NO , error);
            
        } else {
            NSError *errorJson=nil;
            NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorJson];
            
            responseBlock(responseDict , YES , nil);
        }
    }];
    [dataTask resume];
}

+(void)getFavoriteWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock {
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",kBASE_URL , kFavorite] ;
    
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"27ade19e-28c9-62d5-84e7-2559376ce068" };
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"%@", error);
            responseBlock(nil , NO , error);
        } else {
            NSError *errorJson=nil;
            NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorJson];
            responseBlock(responseDict , YES , nil);
        }
    }];
    [dataTask resume];
}



+(void)addFavoriteWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock {
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",kBASE_URL , kFavoriteAdd] ;
    
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"27ade19e-28c9-62d5-84e7-2559376ce068" };
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"%@", error);
            responseBlock(nil , NO , error);
        } else {
            NSError *errorJson=nil;
            NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorJson];
            responseBlock(responseDict , YES , nil);
        }
    }];
    [dataTask resume];
}




+(void)setUserInDefaults :(NSDictionary *)userDictionary{
    [[NSUserDefaults standardUserDefaults] setObject:userDictionary forKey:kUserKey];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

+(NSDictionary *)getUserInDefaults{
    NSDictionary * myDictionary = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kUserKey];
    if (myDictionary) {
        return myDictionary;
    }
    else{
        return nil;
    }
}

+(void)setApplicantInfoInDefaults :(NSString *)userDictionary{
    [[NSUserDefaults standardUserDefaults] setObject:userDictionary forKey:kApplicantInfoKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

+(NSString *)getApplicantInfoInDefaults{
    NSString * myDictionary = [[NSUserDefaults standardUserDefaults] objectForKey:kApplicantInfoKey];
    if (myDictionary) {
        return myDictionary;
    }
    else{
        return nil;
    }
}


+(void)setAuthTokenInDefaults :(NSString *)psuhToken{
    [[NSUserDefaults standardUserDefaults] setObject:psuhToken forKey:kAuthToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

+(NSString *)getAuthTokenInDefaults{
    NSString * psuhToken = [[NSUserDefaults standardUserDefaults] objectForKey:kAuthToken];
    if (psuhToken) {
        return psuhToken;
    }
    else{
        return nil;
    }
}





+(void)setPushTokenInDefaults :(NSString *)psuhToken{
    [[NSUserDefaults standardUserDefaults] setObject:psuhToken forKey:kDeviceToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

+(NSString *)getPushTokenInDefaults{
    NSString * psuhToken = [[NSUserDefaults standardUserDefaults] objectForKey:kDeviceToken];
    if (psuhToken) {
        return psuhToken;
    }
    else{
        return nil;
    }
}

+(void)setUserProfileInDefaults :(NSString *)userDictionary{
    [[NSUserDefaults standardUserDefaults] setObject:userDictionary forKey:kUserProfileKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

+(NSString *)getUserProfileInDefaults{
    NSString * myDictionary = [[NSUserDefaults standardUserDefaults] objectForKey:kUserProfileKey];
    if (myDictionary) {
        return myDictionary;
    }
    else{
        return nil;
    }
}


+(void)setPushSwitchStateInDefaults :(BOOL)state{
    [[NSUserDefaults standardUserDefaults] setBool:state forKey:kPushState];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(BOOL)getPushSwitchStateInDefaults{
    BOOL pushState = [[NSUserDefaults standardUserDefaults] boolForKey:kPushState];
    return pushState;
}


+(BOOL) NSStringIsValidEmail:(NSString *)checkString{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
    
+(void)showGlobalActivity {
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}
    
+(void)dismissGlobalActivity {
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}
+(BOOL)checkLoader {
    return [[UIApplication sharedApplication] isIgnoringInteractionEvents];
}

+(NSArray *)getFistAndLastNameFromFullName : (NSString *)fullName {
    NSArray* firstLastStrings = [fullName componentsSeparatedByString:@" "];
    NSString* firstName = [firstLastStrings objectAtIndex:0];
    NSString* lastName ;
    if (firstLastStrings.count > 1) {
       lastName = [firstLastStrings objectAtIndex:1];
    }
    else{
        lastName = @" ";
    }
    return [NSArray arrayWithObjects:firstName, lastName, nil];
}


+(NSDate *)getDateFromString :(NSString *)dateString {
    NSDateFormatter *dateFormatter=[NSDateFormatter new];
    //"2016-12-16T20:46:42.181003Z"
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    
    NSRange range = [dateString rangeOfString:@"."];
    dateString = [dateString substringToIndex:range.location];
    
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date=[dateFormatter dateFromString:dateString];
    return date;
}

+(NSMutableString *)getdifferenceInDates :(NSDate *)dateT {
    NSMutableString *timeLeft = [[NSMutableString alloc]init];
    
    NSDate *today10am =[NSDate date];
    
    NSInteger seconds = [dateT timeIntervalSinceDate:today10am];
    
    NSInteger days = (int) (floor(seconds / (3600 * 24)));
    if(days) seconds -= days * 3600 * 24;
    
    NSInteger hours = (int) (floor(seconds / 3600));
    if(hours) seconds -= hours * 3600;
    
    NSInteger minutes = (int) (floor(seconds / 60));
    if(minutes) seconds -= minutes * 60;
    
    if(days) {
        [timeLeft appendString:[NSString stringWithFormat:@"%ldd", (long)days*-1]];
        return timeLeft;
    }
    
    if(hours) {
        [timeLeft appendString:[NSString stringWithFormat: @"%ldh", (long)hours*-1]];
        return timeLeft;
    }
    
    if(minutes) {
        [timeLeft appendString: [NSString stringWithFormat: @"%ldm",(long)minutes*-1]];
        return timeLeft;
    }
    
    if(seconds) {
        [timeLeft appendString:[NSString stringWithFormat: @"%lds", (long)seconds*-1]];
        return timeLeft;
    }
    
    return timeLeft;
}

+(NSMutableString *)getExtendedDifferenceInDates :(NSDate *)dateT {
    NSMutableString *timeLeft = [[NSMutableString alloc]init];
    
    NSDate *today10am =[NSDate date];
    
    NSInteger seconds = [dateT timeIntervalSinceDate:today10am];
    
    NSInteger days = (int) (floor(seconds / (3600 * 24)));
    if(days) seconds -= days * 3600 * 24;
    
    NSInteger hours = (int) (floor(seconds / 3600));
    if(hours) seconds -= hours * 3600;
    
    NSInteger minutes = (int) (floor(seconds / 60));
    if(minutes) seconds -= minutes * 60;
    
    if(days) {
        [timeLeft appendString:[NSString stringWithFormat:@"%ld days ago", (long)days*-1]];
        return timeLeft;
    }
    
    if(hours) {
        [timeLeft appendString:[NSString stringWithFormat: @"%ld hours ago", (long)hours*-1]];
        return timeLeft;
    }
    
    if(minutes) {
        [timeLeft appendString: [NSString stringWithFormat: @"%ld minutes ago",(long)minutes*-1]];
        return timeLeft;
    }
    
    if(seconds) {
        [timeLeft appendString:[NSString stringWithFormat: @"%ld seconds ago", (long)seconds*-1]];
        return timeLeft;
    }
    
    return timeLeft;
}


+(void)uploadToAmazonS3WithData :(NSData *)data AndResponseBlock:(APIRequestResponseBlock)responseBlock AndProgressBlock :(APIProgressBlock)progress{

    AWSStaticCredentialsProvider* credentialsProvider = [[AWSStaticCredentialsProvider alloc] initWithAccessKey:@"AKIAJBOEQASD4L7YUKFQ" secretKey:@"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"];
    
    AWSServiceConfiguration* configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSEast1 credentialsProvider:credentialsProvider];

    [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;

    NSString* mimeType =  @"image/jpeg";
    NSString* fileExtension = @".png";

    NSString* fileName = [NSString stringWithFormat:@"%@%@",[self createRandomName],fileExtension];
    
    
    NSURL* fileURL = [[NSURL fileURLWithPath:NSTemporaryDirectory()] URLByAppendingPathComponent:fileName];
    
    
    [data writeToURL:fileURL atomically:YES];
    
    
    AWSS3TransferManagerUploadRequest* uploadRequest = [[AWSS3TransferManagerUploadRequest alloc] init];
    
    uploadRequest.body = fileURL;
    uploadRequest.key = fileName;
    uploadRequest.bucket = @"uchoose";
    uploadRequest.contentType = mimeType;
    uploadRequest.ACL = AWSS3ObjectCannedACLPublicRead;
    
    uploadRequest.uploadProgress = ^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
        CGFloat percentageUploaded = (totalBytesSent/totalBytesExpectedToSend) * 100;
        progress(percentageUploaded);
    };

    AWSS3TransferManager* transferManager = [AWSS3TransferManager defaultS3TransferManager];
    [[transferManager upload:uploadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id _Nullable(AWSTask * _Nonnull task) {
        if (task.error) {
            responseBlock(nil , NO , task.error);
        }
        else{
            NSURL* url = [AWSS3 defaultS3].configuration.endpoint.URL;
            NSURL* publicURL = [[url URLByAppendingPathComponent:uploadRequest.bucket] URLByAppendingPathComponent:uploadRequest.key];
            NSDictionary *dict = @{ @"url" : publicURL.absoluteString};
            responseBlock(dict , YES , nil);
        }
        return nil;
    }];
    

}


+ (NSString *)createRandomName{
    NSTimeInterval timeStamp = [ [ NSDate date ] timeIntervalSince1970 ];
    NSString *randomName = [ NSString stringWithFormat:@"M%f", timeStamp];
    randomName = [ randomName stringByReplacingOccurrencesOfString:@"." withString:@"" ];
    return randomName;
}


-(void)registerToPush {
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( !error ){
                [[UIApplication sharedApplication] registerForRemoteNotifications];
                
                
            }
            else{
                NSLog(@"Error at device push");
            }
        }];
    }
    else{
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}

-(void)unRegesterPush {
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *token =
    [[[[deviceToken description] stringByReplacingOccurrencesOfString:@"<"
                                                           withString:@""]
      stringByReplacingOccurrencesOfString:@">"
      withString:@""]
     stringByReplacingOccurrencesOfString:@" "
     withString:@""];
    [Utility setPushTokenInDefaults:token];
}

- (void)tokenRefreshNotification:(NSNotification *)notification {
    
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    
    NSLog(@"%@" , error.description);
    
}
+(NSString *)getYearFromDate :(NSString *)dateFull {
    NSString *prefix = nil;
    
    if ([dateFull length] >= 4)
        prefix = [dateFull substringToIndex:4];
    else
        prefix = dateFull;
    
    return prefix;
}

+ (void)setupBackButton:(UIViewController *)viewController {
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] init];
    backBarButtonItem.title = @"Back";
    viewController.navigationItem.backBarButtonItem = backBarButtonItem;
}

+ (NSString *)getStateInString:(NSNumber *)state {
    if ([state intValue] == 0) {
        return @"DRAFT";
    }
    else if ([state intValue] == 1){
        return @"IN_REVIEW";
    }
    else if ([state intValue] == 2){
        return @"APPROVED";
    }
    else if ([state intValue] == 3){
        return @"LIVE";
    }
    else if ([state intValue] == 4){
        return @"LIVE_PAUSED";
    }
    else if ([state intValue] == 5){
        return @"LIVE_SUSPENDED";
    }
    else if ([state intValue] == 6){
        return @"TERMINATED";
    }
    else{
        NSLog(@"Wrong State Coming");
        return @"";
    }
}

+ (NSString *)getStateInStringSmall:(NSNumber *)state {
    if ([state intValue] == 0) {
        return @"draft";
    }
    else if ([state intValue] == 1){
        return @"in review";
    }
    else if ([state intValue] == 2){
        return @"approved";
    }
    else if ([state intValue] == 3){
        return @"live";
    }
    else if ([state intValue] == 4){
        return @"live paused";
    }
    else if ([state intValue] == 5){
        return @"live suspended";
    }
    else if ([state intValue] == 6){
        return @"terminated";
    }
    else{
        NSLog(@"Wrong State Coming");
        return @"";
    }
}




+ (void)listAllProjectsFonts {
    NSArray *familyNames = [UIFont familyNames];
    [familyNames enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
        NSLog(@"* %@",obj);
        NSArray *fontNames = [UIFont fontNamesForFamilyName:obj];
        [fontNames enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
            NSLog(@"--- %@",obj);
        }];
    }];
}

+(void)hebrewDateTest {
    NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSCalendar * hebrew = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierHebrew];
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterLongStyle];
    [formatter setTimeStyle:NSDateFormatterNoStyle];
    [formatter setCalendar:hebrew];
    [formatter setDateFormat:@"yyyy-MMM-dd"];
    NSString *hebrewStr = [formatter stringFromDate:[NSDate date]];
    NSLog(@"hebrew: %@", hebrewStr);
    NSLog(@"hebrew DATE : %@", [self convert:hebrewStr]);
    for(int months = 0; months < 12; months++){
        NSDate *today = [hebrew dateByAddingUnit:NSCalendarUnitMonth value:months toDate:[NSDate date] options:0];
        NSLog(@"hebrew: %@", [formatter stringFromDate:today]);
    }
    /*
     
     5778-Av-18
     5778-Elul-18
     5779-Tishri-18
     5779-Heshvan-18
     5779-Kislev-18
     5779-Tevet-18
     5779-Shevat-18
     5779-Adar I-18
     5779-Adar II-18
     5779-Nisan-18
     5779-Iyar-18
     5779-Sivan-18
     UNUserNotificationCenter.current().getDeliveredNotifications { (requests) in
     for request  in  requests {
     print("Delivered on\(request.date)")
     }
     }
     */
    [[UNUserNotificationCenter currentNotificationCenter] getDeliveredNotificationsWithCompletionHandler:^(NSArray<UNNotification *> * _Nonnull notifications) {
        for(UNNotification *notification in notifications){
            NSLog(@"Delived on %@" , notification.date);
        }
    }];
    [[UNUserNotificationCenter currentNotificationCenter] getPendingNotificationRequestsWithCompletionHandler:^(NSArray<UNNotificationRequest *> * _Nonnull requests) {
        NSLog(@"Pending Count %lu" , (unsigned long)requests.count);
        for(UNNotificationRequest *notificationRequest in requests){
            if([[notificationRequest trigger] isKindOfClass:[UNTimeIntervalNotificationTrigger class]]){
                UNTimeIntervalNotificationTrigger *requestTrigger = (UNTimeIntervalNotificationTrigger *)[notificationRequest trigger];
                NSLog(@"Will Deliver on %@" , [requestTrigger nextTriggerDate]);
            }
            if([[notificationRequest trigger] isKindOfClass:[UNCalendarNotificationTrigger class]]){
                UNCalendarNotificationTrigger *requestTrigger = (UNCalendarNotificationTrigger *)[notificationRequest trigger];
                NSLog(@"D-%@" , [requestTrigger nextTriggerDate]);
            }
        }
    }];
    
    [[UNUserNotificationCenter currentNotificationCenter] removeAllDeliveredNotifications];
    
}

+(NSDate*)convert:(NSString*)strdate{
    strdate = [NSString stringWithFormat:@"%@ 11-30-00",strdate];
    NSCalendar * hebrew = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierHebrew];
    NSDateFormatter *sdateFormatter = [[NSDateFormatter alloc] init];
    [sdateFormatter setCalendar:hebrew];
    sdateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [sdateFormatter setDateFormat:@"yyyy-MMM-dd hh-mm-ss"];
    NSDate *sdate = [sdateFormatter dateFromString:strdate];
    return sdate;
}















#pragma mark - Used Functions
+(void)getCurrentDatesIdsAndResponseBlock:(APIRequestResponseBlock)responseBlock{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",kBASE_URL , kGetIdsOnAllCurrentDates] ;
    [manager POST:requestUrl parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSFetchRequest *fetchRequest = [Date fetchRequest];
        NSArray *allDates = [[Utility managedObjectContext] executeFetchRequest:fetchRequest error:nil];
        BOOL isSomeThingDeleted = NO;
        for (Date *date in allDates) {
            BOOL found = NO;
            for (NSDictionary *dict in responseObject) {
                NSNumber *idNumber = [dict objectForKey:@"id"];
                int backendId = [idNumber intValue];
                if (backendId == date.id) {
                    found = YES;
                    break;
                }
            }
            if (!found) {
                if (![date.notificationId containsString:@"Gedol-Manual"]) {
                    NSLog(@"DELETING DATE WITH TITLE = %@", date.subjectTitle);
                    [[Utility managedObjectContext] deleteObject:date];
                    isSomeThingDeleted = YES;
                }
            }
        }
        [[Utility managedObjectContext] save:nil];
        responseBlock(nil , isSomeThingDeleted , nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"ERROR AT THE IDS FETCH = %@" , error.description);
    }];
}

+(void)checkForNewDatesWithLocalHebrewDate {
    NSFetchRequest *fetchRequest = [Date fetchRequest];
    NSArray *maxDates = [[Utility managedObjectContext] executeFetchRequest:fetchRequest error:nil];
    //request.predicate = [NSPredicate predicateWithFormat:@"personId==max(personId)"];

    NSArray *sortedEventArray = [maxDates sortedArrayUsingComparator:^NSComparisonResult(Date *date1, Date *date2) {
        return [date1.modified_date compare:date2.modified_date];
    }];
    NSString *lastModifiedDate = @"1970-06-07T16:50:06.544459Z";
    
    if (sortedEventArray.count == 0) {
        lastModifiedDate = @"1970-06-07T16:50:06.544459Z";
        NSString *pathYears = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Dates-2019-01-21.json"];
        if ([[NSFileManager defaultManager] fileExistsAtPath:pathYears])
        {
            NSArray *design = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:pathYears] options:kNilOptions error:nil];
            NSMutableArray *subjectArray = [[NSMutableArray alloc] init];
            for (NSDictionary *subjectDict in design) {
                NSError *errorModeling;
                SubjectModel *subjectModel = [[SubjectModel alloc] initWithDictionary:subjectDict error:&errorModeling];
                if (errorModeling) {
                    NSLog(@"ERROR in Modelibng = %@" , errorModeling.description);
                }
                else{
                    [subjectArray addObject:subjectModel];
                }
            }
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self serverDatesForSaving:[[NSArray<SubjectModel> alloc] initWithArray:subjectArray]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self checkForNewDatesWithLocalHebrewDate];
                });
            });
        }
        
    }
    else{
        Date *maxDateObj = [sortedEventArray lastObject];
        NSLog(@"MAX DATE IS = %@" , maxDateObj.modified_date);
        NSString * const kAPIDateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSSSSS";
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:kAPIDateFormat];
        lastModifiedDate = [NSString stringWithFormat:@"%@Z",[formatter stringFromDate:[maxDateObj.modified_date dateByAddingTimeInterval:1.0]]];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        NSDictionary *dict = @{ @"modified_date" : lastModifiedDate, @"page" : @"-1"};
        
        NSLog(@"lastModifiedDate IS = %@" , lastModifiedDate);

        
        [self getSubjectsBeforeDateWithJSONWithDictionary:dict AndResponseBlock:^(id object, BOOL status, NSError *error) {
            if (error) {
                NSLog(@"ERROR ON BEFORE DATE = %@",error.description);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self renewNotificationsDates];
                });
                return;
            }
            SubjectDateModel *allDateSubjects = (SubjectDateModel*)object;
            [self serverDatesForSaving:allDateSubjects.Dates];
        }];
    }
    
}

+(void)serverDatesForSaving : (NSArray<SubjectModel> *)allDateSubjects {
    if(allDateSubjects.count == 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self renewNotificationsDates];
        });
    }
    
    
    NSCalendar * hebrew = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierHebrew];
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterLongStyle];
    [formatter setTimeStyle:NSDateFormatterNoStyle];
    [formatter setCalendar:hebrew];
    [formatter setDateFormat:@"yyyy-MMM-dd"];
    int hebrewYear = [[formatter stringFromDate:[NSDate date]] intValue];
    
    NSDictionary *monthsDict = @{@1 : @"Nisan",
                                 @2 : @"Iyar" ,
                                 @3 : @"Sivan",
                                 @4 : @"Tamuz",
                                 @5 : @"Av",
                                 @6 : @"Elul",
                                 @7 : @"Tishri",
                                 @8 : @"Heshvan",
                                 @9 : @"Kislev",
                                 @10 : @"Tevet",
                                 @11 : @"Shevat",
                                 @12 : @"Adar I",
                                 @13 : @"Adar II"
                                 };
    
    
    NSDictionary *allSubjectsGrouped = [allDateSubjects linq_groupBy:^id(SubjectModel *subject) {
        return [NSString stringWithFormat:@"%@-%@" ,subject.day , subject.month ];
    }];
    
    NSLog(@"ALL KEYS COUNT = %lu" , (unsigned long)[allSubjectsGrouped allKeys].count);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    });
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSMutableArray *datesAndTitles = [[NSMutableArray alloc] init];
        
        for (NSString *subjectKey in [allSubjectsGrouped allKeys]) {
            NSArray *singleSubjectGroup = [allSubjectsGrouped objectForKey:subjectKey];
            SubjectModel *subjectForDate = [singleSubjectGroup firstObject];
            __block NSString *subjectTitles = subjectForDate.subjectTitle;
            
            
            NSString *hebrewMonthName = monthsDict[subjectForDate.month] ;
            NSLog(@"HEBREW YEAR is %@" , hebrewMonthName);
            
            
            
            NSString *hebrewYearStr = [NSString stringWithFormat:@"%i-%@-%i",hebrewYear, hebrewMonthName, [subjectForDate.day intValue]];
            NSDate *convertedDate = [self convert:hebrewYearStr];
            NSLog(@"HEBREW YEAR is convertedDate %@" , convertedDate);

             if([convertedDate compare: [NSDate date]] == NSOrderedAscending){
             NSLog(@"Date has been passed... %@" , convertedDate);
             hebrewYearStr = [NSString stringWithFormat:@"%i-%@-%i",(hebrewYear + 1), hebrewMonthName, [subjectForDate.day intValue]];
             convertedDate = [self convert:hebrewYearStr];
             }//TODO
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            SubjectModel *lastSubject = [singleSubjectGroup lastObject];
            NSString *notificationStr = [NSString stringWithFormat:@"%@-Gedol",subjectKey];
            
            
            
            for (SubjectModel *subject in singleSubjectGroup) {
                if (![subject isEqual:subjectForDate]) {
                    subjectTitles = [NSString stringWithFormat:@"%@, %@" ,subjectTitles , subject.subjectTitle];
                }
                NSFetchRequest *fetchRequest = [Date fetchRequest];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %i",[subject.id intValue]];
                fetchRequest.predicate = predicate;
                NSArray *foundDates = [[Utility managedObjectContext] executeFetchRequest:fetchRequest error:nil] ;
                
                
                Date *newDate;
                if(foundDates != nil){
                    if (foundDates.count) {
                        newDate = [foundDates firstObject];
                    }
                    else{
                        newDate = [[Date alloc] initWithContext:[Utility managedObjectContext]];
                        NSLog(@"NEW DATE IS %@" , subject.subjectTitle);
                    }
                }
                else{
                    newDate = [[Date alloc] initWithContext:[Utility managedObjectContext]];
                    NSLog(@"NEW DATE IS %@" , subject.subjectTitle);
                }
                newDate.id = [subject.id intValue];
                newDate.day = [subject.day intValue];
                newDate.month = [subject.month intValue];
                newDate.subjectTitle = subject.subjectTitle;
                newDate.subjectDescription = subject.subjectDescription;
                NSString * const kAPIDateFormat = @"yyyy-MM-dd HH:mm:ss";//2018-08-10 17:08:40
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:kAPIDateFormat];
                NSString *createdDateStr = subject.created_date;
                NSString *modifiedDateStr = subject.modified_date;
                if (createdDateStr.length > 19) {
                    createdDateStr = [createdDateStr substringToIndex:19];
                    createdDateStr = [createdDateStr stringByReplacingOccurrencesOfString:@"T" withString:@" "];
                }
                if (modifiedDateStr.length > 19) {
                    modifiedDateStr = [modifiedDateStr substringToIndex:19];
                    modifiedDateStr = [modifiedDateStr stringByReplacingOccurrencesOfString:@"T" withString:@" "];
                }
                newDate.created_date =  [formatter dateFromString:createdDateStr];
                newDate.modified_date =  [formatter dateFromString:modifiedDateStr];
                NSLog(@"CREATED = %@ , %@",createdDateStr,newDate.created_date);
                NSLog(@"MODIFIED = %@ , %@",modifiedDateStr,newDate.modified_date);
                
                //newDate.modified_date =  [NSDate dateWithTimeInterval:1.0 sinceDate:[formatter dateFromString:modifiedDateStr]];
                if(![NSDate dateWithTimeInterval:1.0 sinceDate:[formatter dateFromString:modifiedDateStr]]){
                    NSLog(@"VERY BIG ISSUE");
                }
                
                newDate.notificationId = notificationStr;
                newDate.grDate = convertedDate;
                if ([subject isEqual:lastSubject]) {
                    if ([self compareDatesWithoutTimeWithDate:convertedDate] == NSOrderedAscending) {
                        NSLog(@"Date Passed");
                    }
                    else{
                        NSDictionary *dict = @{ @"date" : convertedDate, @"title" : subjectTitles, @"notification" : notificationStr};
                        [datesAndTitles addObject:dict];
                    }
                }
                
            }

            if ([subjectKey isEqual:[[allSubjectsGrouped allKeys] lastObject]]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                });
            }
        }
        
        
        datesAndTitles = [[datesAndTitles sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            return [[obj1 valueForKey:@"date"] compare:[obj2 valueForKey:@"date"]];
        }] mutableCopy];
        
        
        int datesCount = 40;
        if (datesAndTitles.count < datesCount) {
            datesCount = (int)datesAndTitles.count;
        }
        if(datesAndTitles.count > 0){
            NSDictionary *dict = [datesAndTitles objectAtIndex:0];
            [self fireNotificationForDates:[dict valueForKey:@"date"] andTitles:[dict valueForKey:@"title"] andId:[dict valueForKey:@"notification"] andIndex:0 andAllDatesDicts:datesAndTitles andManual:NO];
            
        }
    });
    
    __block NSError *errorCoreData;
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [[Utility managedObjectContext] save:&errorCoreData];
    });
    if(errorCoreData){
        NSLog(@"ERROR IN COREDATA = %@" , errorCoreData.description);
    }
    

}



+(void)getSubjectsBeforeDateWithJSONWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock {
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    //NSString *requestUrl = [NSString stringWithFormat:@"%@%@",kBASE_URL , kGetInformationOnDate] ;
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",kBASE_URL , kGetInformationBeforeDate] ;
    [manager POST:requestUrl parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError *errorDecode;
        SubjectDateModel *allDateSubjects = [[SubjectDateModel alloc] initWithDictionary:responseObject error:&errorDecode];
        if (errorDecode) {
            NSLog(@"ERROR ON PYTHON DECODE = %@", errorDecode.description);
            responseBlock(nil , NO , errorDecode);
        }
        else{
            responseBlock(allDateSubjects , YES , nil);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        responseBlock(nil , NO , error);
    }];
}

+(void)fireNotificationForDates :(NSDate *)date andTitles :(NSString *)titles andId :(NSString *)notificationId andIndex :(int)index andAllDatesDicts :(NSMutableArray *)allDatesDicts andManual :(BOOL)isManual{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat: @"dd-MM-yyyy"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    NSLog(@"____%@" , stringFromDate);
    
    
    UNMutableNotificationContent *objNotificationContent = [[UNMutableNotificationContent alloc] init];
    objNotificationContent.title = [NSString localizedUserNotificationStringForKey:@"Today's Yartzeits!" arguments:nil];
    objNotificationContent.body = [NSString localizedUserNotificationStringForKey:titles arguments:nil];
    objNotificationContent.sound = [UNNotificationSound defaultSound];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:date];
    
    /*
     if (components.month == 6 && components.day == 15) {
     components.day = 14;
     
     }
     */
    
    components.hour = 8;
    components.minute = 00;
    components.second = 00;
    
        
    
    NSMutableArray *yourArray = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"NotifiedDates"] mutableCopy];
    if(!yourArray){
        yourArray = [[NSMutableArray alloc] init];
    }
    
    if ([yourArray containsObject:date]) {
        if (isManual) {
            NSLog(@"DATE ALREADY CONTAINED");
        }
        else{
            return;
        }
        
    }
    if (isManual) {
        
    }
    else{
        if (yourArray.count >= 40) {
            NSLog(@"50 Limit....");
            return;
        }
    }
    
    
    date = [[NSCalendar currentCalendar] dateFromComponents:components];
    
    [[TestSQB alloc] scheduleNotificationWithTitles:titles andIdentifier:notificationId andDate:date];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if(yourArray.count == 0){
            NSLog(@"NEED TO ADD HERE");
        }
        [yourArray addObject:date];
        [[NSUserDefaults standardUserDefaults] setObject:yourArray forKey:@"NotifiedDates"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSLog(@"Local Notification succeeded %lu", (unsigned long)yourArray.count);
        
        if (isManual) {
            return ;
        }
        
        int datesCount = 50;
        if (allDatesDicts.count < datesCount) {
            datesCount = (int)allDatesDicts.count;
        }
        if(index+1 < datesCount){
            NSDictionary *dict = [allDatesDicts objectAtIndex:index+1];
            
            [self fireNotificationForDates:[dict valueForKey:@"date"] andTitles:[dict valueForKey:@"title"] andId:[dict valueForKey:@"notification"] andIndex:index+1 andAllDatesDicts:allDatesDicts andManual:NO];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        }
        else{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSMutableArray *yourArray2 = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"NotifiedDates"] mutableCopy];
            NSLog(@"yourArray COUNT = %lu" , (unsigned long)yourArray2.count);
            
        }
    });
}


+(void)renewNotificationsDates {
    NSDictionary *monthsDict = @{@1 : @"Nisan",
                                 @2 : @"Iyar" ,
                                 @3 : @"Sivan",
                                 @4 : @"Tamuz",
                                 @5 : @"Av",
                                 @6 : @"Elul",
                                 @7 : @"Tishri",
                                 @8 : @"Heshvan",
                                 @9 : @"Kislev",
                                 @10 : @"Tevet",
                                 @11 : @"Shevat",
                                 @12 : @"Adar I",
                                 @13 : @"Adar II"
                                 };
    NSCalendar * hebrew = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierHebrew];
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterLongStyle];
    [formatter setTimeStyle:NSDateFormatterNoStyle];
    [formatter setCalendar:hebrew];
    [formatter setDateFormat:@"yyyy-MMM-dd"];
    int hebrewYear = [[formatter stringFromDate:[NSDate date]] intValue];
    
    
    NSMutableArray *yourArray = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"NotifiedDates"] mutableCopy];
    yourArray = [[yourArray sortedArrayUsingComparator:^NSComparisonResult(NSDate *obj1, NSDate *obj2) {
        return [obj1 compare:obj2];
    }] mutableCopy];
    
    NSLog(@"yourArray COUNT = %lu" , (unsigned long)yourArray.count);
    
    NSMutableArray *newArray = [[NSMutableArray alloc] init];
    
    for (NSDate *date in yourArray) {
        if([date compare: [NSDate date]] == NSOrderedDescending) // if start is later in time than end
        {
            // do something
            [newArray addObject:date];
        }
        
    }
    [[NSUserDefaults standardUserDefaults] setObject:newArray forKey:@"NotifiedDates"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    NSFetchRequest *fetchRequest = [Date fetchRequest];
    NSMutableArray *allDates = [[[Utility managedObjectContext] executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    allDates = [[allDates sortedArrayUsingComparator:^NSComparisonResult(Date *obj1, Date *obj2) {
        return [obj1.grDate compare:obj2.grDate];
    }] mutableCopy];
    
    
    
    for (Date *oldDate in allDates) {
        if ([self compareDatesWithoutTimeWithDate:oldDate.grDate] == NSOrderedAscending) {
            NSLog(@"Date Passed");
            NSString *hebrewMonthName = monthsDict[[NSNumber numberWithInt:oldDate.month]] ;
            NSString *hebrewYearStr = [NSString stringWithFormat:@"%i-%@-%i",hebrewYear, hebrewMonthName, oldDate.day];
            NSDate *convertedDate = [self convert:hebrewYearStr];
            
            if([convertedDate compare: [NSDate date]] == NSOrderedAscending){
                NSLog(@"Date has been passed... %@" , convertedDate);
                hebrewYearStr = [NSString stringWithFormat:@"%i-%@-%i",(hebrewYear + 1), hebrewMonthName, oldDate.day];
                convertedDate = [self convert:hebrewYearStr];
                oldDate.grDate = convertedDate;
            }
        }
    }
    __block NSError *error;
    dispatch_async(dispatch_get_main_queue(), ^{
        [[Utility managedObjectContext] save:&error];
    });
    int moreDatesAddCount = 40-(int)newArray.count;
    NSLog(@"MORE DATES CAN BE ADDDED = %i" , moreDatesAddCount);
    if(moreDatesAddCount > 0){
        NSLog(@"MORE DATES CAN BE ADDDED = %i" , moreDatesAddCount);
        
        NSDictionary *allDatesGrouped = [allDates linq_groupBy:^id(Date *date) {
            return [NSString stringWithFormat:@"%hd-%hd" ,date.day , date.month];
        }];
        NSLog(@"ALL KEYS COUNT = %lu" , (unsigned long)[allDatesGrouped allKeys].count);
        NSMutableArray *datesAndTitles = [[NSMutableArray alloc] init];
        
        NSMutableArray *allSubjectKeysSorted = [[allDatesGrouped allKeys] mutableCopy];
        
        allSubjectKeysSorted = [[allSubjectKeysSorted sortedArrayUsingComparator:^NSComparisonResult(NSString *obj1, NSString *obj2) {
            return [obj1 compare:obj1] == NSOrderedAscending;
        }] mutableCopy];

        for (NSString *subjectKey in allSubjectKeysSorted) {
            NSLog(@"subjectKey = %@" , subjectKey);
            NSArray *singleSubjectGroup = [allDatesGrouped objectForKey:subjectKey];
            Date *subjectForDate = [singleSubjectGroup firstObject];
            if ([self compareDatesWithoutTimeWithDate:subjectForDate.grDate] == NSOrderedAscending) {
                NSLog(@"Date Passed");
            }
            else{
                __block BOOL isFound = NO;
                for(NSDate *savedDate in newArray){
                    if([self compareDatesWithoutTimeWithDate:subjectForDate.grDate andOtherDate:savedDate] == NSOrderedSame){
                        isFound = YES;
                        break;
                    }
                }
                if(isFound == NO) {
                    __block NSString *subjectTitles = subjectForDate.subjectTitle;
                    NSString *notificationStr = [NSString stringWithFormat:@"%@-Gedol",subjectKey];
                    
                    for (Date *date in singleSubjectGroup) {
                        if (![date isEqual:subjectForDate]) {
                            subjectTitles = [NSString stringWithFormat:@"%@, %@" ,subjectTitles , date.subjectTitle];
                        }
                    }
                    NSDictionary *dict = @{ @"date" : subjectForDate.grDate, @"title" : subjectTitles, @"notification" : notificationStr};
                    [datesAndTitles addObject:dict];
                    
                    int newMoreDatesAddCount = moreDatesAddCount+(int)datesAndTitles.count;
                    if(newMoreDatesAddCount>=40){
                        [self fireNotificationForDates:[dict valueForKey:@"date"] andTitles:[dict valueForKey:@"title"] andId:[dict valueForKey:@"notification"] andIndex:0 andAllDatesDicts:datesAndTitles andManual:NO];
                        return;
                    }
                }
            }
        }
    }
}

+(NSComparisonResult)compareDatesWithoutTimeWithDate :(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger desiredComponents = (NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear);
    
    NSDate *firstDate = date; // one date
    NSDate *secondDate = [NSDate date]; // the other date
    
    NSDateComponents *firstComponents = [calendar components:desiredComponents fromDate:firstDate];
    NSDateComponents *secondComponents = [calendar components:desiredComponents fromDate:secondDate];
    
    NSDate *truncatedFirst = [calendar dateFromComponents:firstComponents];
    NSDate *truncatedSecond = [calendar dateFromComponents:secondComponents];
    
    NSComparisonResult result = [truncatedFirst compare:truncatedSecond];
    return result;
}

+(NSComparisonResult)compareDatesWithoutTimeWithDate :(NSDate *)date andOtherDate :(NSDate *)otherDate {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger desiredComponents = (NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear);
    
    NSDate *firstDate = date; // one date
    NSDate *secondDate = otherDate; // the other date
    
    NSDateComponents *firstComponents = [calendar components:desiredComponents fromDate:firstDate];
    NSDateComponents *secondComponents = [calendar components:desiredComponents fromDate:secondDate];
    
    NSDate *truncatedFirst = [calendar dateFromComponents:firstComponents];
    NSDate *truncatedSecond = [calendar dateFromComponents:secondComponents];
    
    NSComparisonResult result = [truncatedFirst compare:truncatedSecond];
    return result;
}




+(void)deleteAndRefreshDates {
    NSLog(@"deleteAndRefreshDates STARTING.......");
    NSFetchRequest *fetchRequest = [Date fetchRequest];
    NSArray *allDates = [[Utility managedObjectContext] executeFetchRequest:fetchRequest error:nil];
    for (Date *date in allDates) {
        if (![date.notificationId containsString:@"Gedol-Manual"]) {
            [[Utility managedObjectContext] deleteObject:date];
        }
    }
    [[Utility managedObjectContext] save:nil];
    [[TestSQB alloc] removePendingNotifications];
    [[NSUserDefaults standardUserDefaults] setObject:[[NSMutableArray alloc] init] forKey:@"NotifiedDates"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    
    
    NSString *pathYears = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Dates-2019-01-21.json"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:pathYears])
    {
        NSArray *design = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:pathYears] options:kNilOptions error:nil];
        NSMutableArray *subjectArray = [[NSMutableArray alloc] init];
        for (NSDictionary *subjectDict in design) {
            NSError *errorModeling;
            SubjectModel *subjectModel = [[SubjectModel alloc] initWithDictionary:subjectDict error:&errorModeling];
            if (errorModeling) {
                NSLog(@"ERROR in Modelibng = %@" , errorModeling.description);
            }
            else{
                [subjectArray addObject:subjectModel];
            }
        }
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self serverDatesForSaving:[[NSArray<SubjectModel> alloc] initWithArray:subjectArray]];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self checkForNewDatesWithLocalHebrewDate];
            });
        });
    }
}



+(NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}

@end
