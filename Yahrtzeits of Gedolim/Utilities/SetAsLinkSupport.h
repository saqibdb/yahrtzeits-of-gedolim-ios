//
//  SetAsLinkSupport.h
//  Yahrtzeits of Gedolim
//
//  Created by ibuildx Macbook 1 on 3/13/19.
//  Copyright © 2019 iBuildX. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSMutableAttributedString (SetAsLinkSupport)

- (BOOL)setAsLink:(NSString*)textToFind linkURL:(NSString*)linkURL;

@end

NS_ASSUME_NONNULL_END
