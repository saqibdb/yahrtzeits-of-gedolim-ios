//
//  SQBConstants.h
//  SQBEmojiSDK
//
//  Created by iBuildx_Mac_Mini on 12/1/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//http://127.0.0.1:8000/
//https://pure-meadow-85640.herokuapp.com
#ifndef SQBConstants_h
#define SQBConstants_h
#define kBASE_URL @"https://yahrtzeits-of-gedolim.herokuapp.com/"
//#define kBASE_URL @"http://03de5381.ngrok.io/"
//#define kBASE_URL @"https://pure-meadow-85640.herokuapp.com/"



#define kGetInformationOnDate @"get_information_on_date/"
#define kGetInformationBeforeDate @"get_information_on_all_date/"
#define kGetIdsOnAllCurrentDates @"get_ids_on_all_current_date/"




#define kLogin @"sing_in/"
#define kRadius @"inradius/"
#define kUpdate @"update_user/"
#define kUpload @"upload/"
#define kFavorite @"all_favorites/"
#define kFavoriteAdd @"favorites/"


#define kSignUp @"signup/"
#define kRegisterPush @"device/pns/register/"
#define kForgetPassword @"password/forget/"
#define kResetPassword @"password/update/"
#define kUserOpportunities @"opportunities/query/"
#define kApplicantInfo @"applicant/query/"
#define kUserProfile @"profile/"
#define kUpdateSummary @"applicant/summary/update/"
#define kAddExperience @"applicant/experience/add/"
#define kUpdateExperience @"applicant/experience/update/"
#define kRemoveExperience @"applicant/experience/remove/"
#define kAddEducation @"applicant/education/add/"
#define kUpdateEducation @"applicant/education/update/"
#define kRemoveEducation @"applicant/education/remove/"

#define kOppurtunityDetail @"/detail/"

#define kSkillsList @"skills/query/"
#define kAttributionList @"attributions/query/"
#define kComments @"comments/query/"
#define kaddComment @"comment/add/"
#define kApply @"apply/"
#define kVote @"vote/"
#define kDetail @"detail/"
#define kCreateOpp @"opportunity/create/"
#define kUpdateOpp @"update/"

#define kEmailLink @"email/link/"
#define kUpdateState @"state/update/"



#define kApplicants @"applicants/query/"

#define kMembership @"membership/"




#define kUserAppliedOpportunities @"opportunities/applied/query/"

//https://www.openworkr.com/m/opportunity/3/detail/

#define kExperienceCellHeight 90
#define kEducationCellHeight 60

#define kUserKey @"Saved_User"
#define kUserProfileKey @"Current_User_Profile"
#define kApplicantInfoKey @"Applicant_Info"

#define kDeviceToken @"Device_Token"
#define kPushState @"Push_State"

#define kAuthToken @"Auth_Token"



#endif /* SQBConstants_h */
