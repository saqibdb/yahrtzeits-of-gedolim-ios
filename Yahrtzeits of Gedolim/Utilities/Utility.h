//
//  Utility.h
//  OpenWorkr
//
//  Created by iBuildx_Mac_Mini on 12/15/16.
//  Copyright © 2016 Eden. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"
#import <UserNotifications/UserNotifications.h>
#import "Date+CoreDataClass.h"
#import "Date+CoreDataProperties.h"
#import <CoreData/CoreData.h>



typedef void (^APIRequestResponseBlock) (id object, BOOL status, NSError *error);//block as a typedef

typedef void (^APIProgressBlock) (CGFloat progress);//block as a typedef


typedef void(^addressCompletion)(NSString *);

@interface Utility : NSObject<UIApplicationDelegate, UNUserNotificationCenterDelegate>

+(void)getHebrewDateFromLink:(NSString *)linkString AndResponseBlock:(APIRequestResponseBlock)responseBlock;

+(void)getSubjectsForDateWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock;

+(void)getSubjectsBeforeDateWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock;


+(NSDate*)convert:(NSString*)strdate;


+(void)fireNotificationForDates :(NSDate *)date andTitles :(NSString *)titles andId :(NSString *)notificationId andIndex :(int)index andAllDatesDicts :(NSMutableArray *)allDatesDicts andManual :(BOOL)isManual;

+(void)checkForNewDates;

+(void)disableNotificationsForAllDates;
+(void)enableNotificationsForAllDates;

+(void)getCurrentDatesIdsAndResponseBlock:(APIRequestResponseBlock)responseBlock;

+(void)loginWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock ;


+(void)loginPostManWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock;
+(void)getUserForMapWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock;
+(void)updateUserWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock;
+(void)uploadProfilePictureWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock;
+(void)getFavoriteWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock;
+(void)addFavoriteWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock ;


+(void)uploadToAmazonS3WithData :(NSData *)data AndResponseBlock:(APIRequestResponseBlock)responseBlock AndProgressBlock :(APIProgressBlock)progress;



+ (NSManagedObjectContext *)managedObjectContext ;



+(BOOL)NSStringIsValidEmail:(NSString *)checkString;
+(void)showGlobalActivity;
+(void)dismissGlobalActivity;
+(BOOL)checkLoader;


+(void)setUserInDefaults :(NSDictionary *)userDictionary;
+(NSDictionary *)getUserInDefaults;

+(void)setAuthTokenInDefaults :(NSString *)psuhToken;
+(NSString *)getAuthTokenInDefaults;

+(void)setPushTokenInDefaults :(NSString *)psuhToken;
+(NSString *)getPushTokenInDefaults;
+(void)setApplicantInfoInDefaults :(NSString *)userDictionary;
+(NSString *)getApplicantInfoInDefaults;

+(void)setUserProfileInDefaults :(NSString *)userDictionary;
+(NSString *)getUserProfileInDefaults;

+(void)setPushSwitchStateInDefaults :(BOOL)state;
+(BOOL)getPushSwitchStateInDefaults;    
    
+(NSArray *)getFistAndLastNameFromFullName : (NSString *)fullName;
+(NSDate *)getDateFromString :(NSString *)dateString;
+(NSMutableString *)getdifferenceInDates :(NSDate *)dateT;
+(NSMutableString *)getExtendedDifferenceInDates :(NSDate *)dateT;

- (void)registerToPush;
- (void)unRegesterPush;
+ (NSString *)getYearFromDate :(NSString *)dateFull;
+ (void)setupBackButton:(UIViewController *)viewController;
+ (NSString *)getStateInString:(NSNumber *)state;
+ (NSString *)getStateInStringSmall:(NSNumber *)state;
+ (void)listAllProjectsFonts;

+(void)hebrewDateTest;
+(void)checkForNewDatesWithLocalHebrewDate;
+(void)deleteAndRefreshDates;
+(NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;
@end
