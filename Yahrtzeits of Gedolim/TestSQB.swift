//
//  TestSQB.swift
//  Yahrtzeits of Gedolim
//
//  Created by iBuildX on 04/06/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

import UIKit
import DLLocalNotifications


@objc class TestSQB: NSObject {
    
    
    @objc public func tester() {
        print("THIS IS TEST")
    }
    
    @objc public func scheduleNotification(withTitles titles : String, andIdentifier notificationId : String, andDate date : NSDate) {
        // The date you would like the notification to fire at
        let triggerDate = date as Date
        
        let firstNotification = DLNotification(identifier: notificationId, alertTitle: "Today's Yartzeits!", alertBody: titles, date: triggerDate, repeats: .None)
        
        let scheduler = DLNotificationScheduler()
        print(scheduler.scheduleNotification(notification: firstNotification)!)
    }
    
    @objc public func printCurrentNotifications() {
//        let scheduler = DLNotificationScheduler()
//        //scheduler.printAllNotifications()
//        scheduler.printDeliveredNotifications()
    }
    @objc public func removePendingNotifications() {
        let scheduler = DLNotificationScheduler()
        scheduler.cancelAlllNotifications()
    }

}
